/* Editable gtktext item type for the GnomeCanvas
 *
 * Copyright (c) 2000 Phil Schwan
 * Copyright (c) 2000 Red Hat Software, Inc.
 *
 * Borrowing heavily from the Tk->Gtk port by Havoc Pennington <hp@redhat.com>
 * Canvas bits by Phil Schwan <pschwan@off.net>
 *
 */

#include "gtktext.h"
#include "gtktextdisplay.h"
#include "gtktextiterprivate.h"
#include <gdk/gdkkeysyms.h>

#include "gtktextcanvas.h"

#include <libgnomeui/gnome-canvas-util.h>

#include <gdk-pixbuf/gdk-pixbuf.h>

#include "math.h"

#if 0
#include <gdk/gdkx.h> /* for BlackPixel */
#include <gdk/gdkkeysyms.h>
#include <gtk/gtksignal.h>

#include "libart_lgpl/art_affine.h"
#include "libart_lgpl/art_rgb.h"
#include "libart_lgpl/art_rgb_bitmap_affine.h"

#include <string.h>
#endif

#include <stdio.h>

/* Object argument IDs */
enum {
	ARG_0,
	ARG_X,
	ARG_Y,
	ARG_NUM_LINES,
	ARG_HEIGHT_LINES,
	ARG_WIDTH_COLUMNS,
	ARG_PIXELS_ABOVE_LINES,
	ARG_PIXELS_BELOW_LINES,
	ARG_PIXELS_INSIDE_WRAP,
	ARG_EDITABLE,
	ARG_WRAP_MODE,
	ARG_FONT_GDK,
	ARG_FG_COLOR_GDK,
	ARG_BG_COLOR_GDK,
	LAST_ARG
};

static void gnome_canvas_tktext_set_arg (GtkObject *object, GtkArg *arg,
				     guint arg_id);
static void gnome_canvas_tktext_get_arg (GtkObject *object, GtkArg *arg,
				     guint arg_id);
static void gnome_canvas_tktext_destroy (GtkObject *object);
static void gnome_canvas_tktext_finalize (GtkObject *object);
static void gnome_canvas_tktext_update (GnomeCanvasItem *item, double *affine,
					ArtSVP *clip_path, int flags);
static void gnome_canvas_tktext_realize (GnomeCanvasItem *item);
static void gnome_canvas_tktext_unrealize (GnomeCanvasItem *item);
static void gnome_canvas_tktext_draw (GnomeCanvasItem *item,
				      GdkDrawable *drawable,
				      int x, int y, int width, int height);
static gint gnome_canvas_tktext_event (GnomeCanvasItem *item, GdkEvent *event);
static void gnome_canvas_tktext_update (GnomeCanvasItem *item, double *affine,
					ArtSVP *clip_path, int flags);
static double gnome_canvas_tktext_point (GnomeCanvasItem *item, double x,
					 double y, int cx, int cy,
					 GnomeCanvasItem **actual_item);
static void gnome_canvas_tktext_stop_cursor_blink(GnomeCanvasTkText *tkxt);
static void gnome_canvas_tktext_start_selection_drag(GnomeCanvasTkText *tkxt,
						     const GtkTextIter *iter,
						     GdkEventButton *event);
static gboolean gnome_canvas_tktext_end_selection_drag(GnomeCanvasTkText *tkxt,
						       GdkEventButton *event);
static void gnome_canvas_tktext_need_repaint(GtkTextLayout *layout, gint x,
					     gint y, gint width, gint height,
					     GnomeCanvasTkText *tkxt);
static gint gnome_canvas_tktext_focus_in_event (GnomeCanvasItem *item,
						GdkEventFocus *event);
static gint gnome_canvas_tktext_focus_out_event(GnomeCanvasItem *item,
						GdkEventFocus *event);

#ifdef TKXT_DND
/* Drag and drop! */
static void gnome_canvas_tktext_start_selection_dnd(GnomeCanvasTkText *tkxt,
						    const GtkTextIter *iter,
						    GdkEventButton *event);
static void gnome_canvas_tktext_drag_end(GnomeCanvasItem *item,
					 GdkDragContext *context);
#endif

/* Key binding fun! */
static void gnome_canvas_tktext_move_insert(GnomeCanvasTkText *tkxt,
					    GtkTextViewMovementStep step,
					    gint count,
					    gboolean extend_selection);
static void gnome_canvas_tktext_set_anchor(GnomeCanvasTkText * tkxt);
static void gnome_canvas_tktext_delete_text(GnomeCanvasTkText *tkxt,
					    GtkTextViewDeleteType type,
					    gint count);
static void gnome_canvas_tktext_cut_text(GnomeCanvasTkText *tkxt);
static void gnome_canvas_tktext_copy_text(GnomeCanvasTkText *tkxt);
static void gnome_canvas_tktext_paste_text(GnomeCanvasTkText *tkxt);
static void gnome_canvas_tktext_toggle_overwrite(GnomeCanvasTkText *tkxt);
/* No more key binding fun. */

static void gnome_canvas_tktext_bounds (GnomeCanvasItem *item, double *x1,
					double *y1, double *x2, double *y2);
#if 0
static void gnome_canvas_tktext_render (GnomeCanvasItem *item,
					GnomeCanvasBuf *buf);
#endif
static void gnome_canvas_tktext_ensure_layout(GnomeCanvasTkText *tkxt);

#if 0
static void gnome_canvas_tktext_class_init (GnomeCanvasTkTextClass *class);
static void gnome_canvas_tktext_init (GtkObject *object);

static GnomeCanvasTextSuckFont *gnome_canvas_suck_font (GdkFont *font);
static void gnome_canvas_suck_font_free (GnomeCanvasTextSuckFont *suckfont);
#endif

static GnomeCanvasItemClass *parent_class;

/* Drag and drop stuff */
enum {
  TARGET_STRING,
  TARGET_TEXT,
  TARGET_COMPOUND_TEXT,
  TARGET_UTF8_STRING
};

#ifdef TKXT_DND
static GtkTargetEntry target_table[] = {
	{ "UTF8_STRING", 0, TARGET_UTF8_STRING },
	{ "COMPOUND_TEXT", 0, TARGET_COMPOUND_TEXT },
	{ "TEXT", 0, TARGET_TEXT },
	{ "text/plain", 0, TARGET_STRING },
	{ "STRING",     0, TARGET_STRING }
};

static guint n_targets = sizeof(target_table) / sizeof(target_table[0]);
#endif

enum {
	FONT_CHANGED,
	FG_COLOR_CHANGED,
	BG_COLOR_CHANGED,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };



/* Class initialization function for the text item */
static void
gnome_canvas_tktext_class_init (GnomeCanvasTkTextClass *class)
{
	GtkObjectClass *object_class;
	GnomeCanvasItemClass *item_class;

	object_class = (GtkObjectClass *) class;
	item_class = (GnomeCanvasItemClass *) class;
	parent_class = gtk_type_class (gnome_canvas_item_get_type ());

	/* FIXME: marshall all TkText arguments */

	gtk_object_add_arg_type ("GnomeCanvasTkText::x",
				 GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_X);
	gtk_object_add_arg_type ("GnomeCanvasTkText::y",
				 GTK_TYPE_DOUBLE, GTK_ARG_READWRITE, ARG_Y);
	gtk_object_add_arg_type ("GnomeCanvasTkText::editable",
				 GTK_TYPE_BOOL, GTK_ARG_READWRITE,
				 ARG_EDITABLE);
	gtk_object_add_arg_type ("GnomeCanvasTkText::num_lines",
				 GTK_TYPE_INT, GTK_ARG_READABLE,
				 ARG_NUM_LINES);
	gtk_object_add_arg_type ("GnomeCanvasTkText::font_gdk",
				 GTK_TYPE_BOXED, GTK_ARG_READWRITE,
				 ARG_FONT_GDK);
	gtk_object_add_arg_type ("GnomeCanvasTkText::fg_color_gdk",
				 GTK_TYPE_BOXED, GTK_ARG_READWRITE,
				 ARG_FG_COLOR_GDK);
	gtk_object_add_arg_type ("GnomeCanvasTkText::bg_color_gdk",
				 GTK_TYPE_BOXED, GTK_ARG_READWRITE,
				 ARG_BG_COLOR_GDK);
#if 0
	gtk_object_add_arg_type ("GnomeCanvasTkText::text_length",
				 GTK_TYPE_INT, GTK_ARG_READABLE,
				 ARG_TEXT_LENGTH);
	gtk_object_add_arg_type ("GnomeCanvasTkText::width",
				 GTK_TYPE_DOUBLE, GTK_ARG_READWRITE,
				 ARG_WIDTH);

	gtk_object_add_arg_type ("GnomeCanvasTkText::anchor",
				 GTK_TYPE_ANCHOR_TYPE, GTK_ARG_READWRITE,
				 ARG_ANCHOR);
	gtk_object_add_arg_type ("GnomeCanvasTkText::justification",
				 GTK_TYPE_JUSTIFICATION, GTK_ARG_READWRITE,
				 ARG_JUSTIFICATION);
#endif

	signals[FONT_CHANGED] =
		gtk_signal_new("font_changed", GTK_RUN_LAST,
			       object_class->type,
			       GTK_SIGNAL_OFFSET(GnomeCanvasTkTextClass,
						 font_changed),
			       gtk_marshal_NONE__POINTER,
			       GTK_TYPE_NONE, 1,
			       GTK_TYPE_POINTER);

	signals[FG_COLOR_CHANGED] =
		gtk_signal_new("fg_color_changed", GTK_RUN_LAST,
			       object_class->type,
			       GTK_SIGNAL_OFFSET(GnomeCanvasTkTextClass,
						 fg_color_changed),
			       gtk_marshal_NONE__INT,
			       GTK_TYPE_NONE, 1,
			       GTK_TYPE_INT);

	signals[BG_COLOR_CHANGED] =
		gtk_signal_new("bg_color_changed", GTK_RUN_LAST,
			       object_class->type,
			       GTK_SIGNAL_OFFSET(GnomeCanvasTkTextClass,
						 bg_color_changed),
			       gtk_marshal_NONE__INT,
			       GTK_TYPE_NONE, 1,
			       GTK_TYPE_INT);

	gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);

	object_class->destroy = gnome_canvas_tktext_destroy;
	object_class->set_arg = gnome_canvas_tktext_set_arg;
	object_class->get_arg = gnome_canvas_tktext_get_arg;
	object_class->finalize = gnome_canvas_tktext_finalize;

	item_class->realize = gnome_canvas_tktext_realize;
	item_class->unrealize = gnome_canvas_tktext_unrealize;
	item_class->update = gnome_canvas_tktext_update;
	item_class->draw = gnome_canvas_tktext_draw;
	item_class->point = gnome_canvas_tktext_point;
	item_class->bounds = gnome_canvas_tktext_bounds;
#if 0
	item_class->render = gnome_canvas_tktext_render;
#endif
	item_class->event = gnome_canvas_tktext_event;
} /* gnome_canvas_tktext_class_init */

/* Object initialization function for the text item */
static void
gnome_canvas_tktext_init (GtkObject *object)
{
	GnomeCanvasTkText *item;

	g_return_if_fail(object);
	g_return_if_fail(GNOME_IS_CANVAS_TKTEXT(object));

	item = GNOME_CANVAS_TKTEXT(object);

	item->x = 0.0;
	item->y = 0.0;
	item->clip_width = 0.0;
	item->clip_height = 0.0;
	item->xofs = 0.0;
	item->yofs = 0.0;
	item->cx = 0.0;
	item->cy = 0.0;
	item->clip_cx = 0.0;
	item->clip_cy = 0.0;
	item->clip_cwidth = 0.0;
	item->clip_cheight = 0.0;
	item->anchor = GTK_ANCHOR_NW;
} /* gnome_canvas_tktext_init */

/**
 * gnome_canvas_tktext_get_type:
 * @void: 
 * 
 * Registers the &GnomeCanvasTkText class if necessary, and returns the type
 * ID associated to it.
 * 
 * Return value: The type ID of the &GnomeCanvasTkText class.
 **/
GtkType
gnome_canvas_tktext_get_type (void)
{
	static GtkType text_type = 0;

	if (!text_type) {
		GtkTypeInfo text_info = {
			"GnomeCanvasTkText",
			sizeof (GnomeCanvasTkText),
			sizeof (GnomeCanvasTkTextClass),
			(GtkClassInitFunc) gnome_canvas_tktext_class_init,
			(GtkObjectInitFunc) gnome_canvas_tktext_init,
			NULL, /* reserved_1 */
			NULL, /* reserved_2 */
			(GtkClassInitFunc) NULL
		};

		text_type = gtk_type_unique (gnome_canvas_item_get_type (),
					     &text_info);
	}

	return text_type;
} /* gnome_canvas_tktext_get_type */

GnomeCanvasItem *
gnome_canvas_tktext_new(void)
{
	return GNOME_CANVAS_ITEM(gtk_type_new(gnome_canvas_tktext_get_type()));
} /* gnome_canvas_tktext_new */

static void
changed_handler(GtkTextBuffer *buffer, GnomeCanvasItem *item)
{
	gnome_canvas_item_request_update(item);
}

static void
gnome_canvas_tktext_emit_cursor_event(GnomeCanvasTkText *tkxt,
				      GtkTextIter *iter, gboolean force)
{
	GtkTextStyleValues *style;
	GtkWidget *canvas;

	g_return_if_fail(tkxt);
	g_return_if_fail(iter);

	canvas = GTK_WIDGET(GNOME_CANVAS_ITEM(tkxt)->canvas);

	/* Emit event when cursor moves */
	style = gtk_text_layout_get_style(tkxt->layout, iter);
#if 0
	gtk_text_view_style_values_realize(style,
					   gtk_widget_get_colormap(canvas),
					   gtk_widget_get_visual(canvas));
#endif

	if (tkxt->last_font != style->font || force) {
		/* blah blah blah */
		printf("would emit font event: %p\n", style->font);
		tkxt->last_font = style->font;
		gtk_signal_emit(GTK_OBJECT(tkxt), signals[FONT_CHANGED],
				tkxt->last_font);
	}
	if (tkxt->last_fg_color != style->fg_color.pixel || force) {
		printf("would emit fgcolor event: %lu\n",
		       style->fg_color.pixel);
		tkxt->last_fg_color = style->fg_color.pixel;
		gtk_signal_emit(GTK_OBJECT(tkxt), signals[FG_COLOR_CHANGED],
				tkxt->last_fg_color);
	}
	if (tkxt->last_bg_color != style->bg_color.pixel || force) {
		printf("would emit bgcolor event: %lu\n",
		       style->bg_color.pixel);
		tkxt->last_bg_color = style->bg_color.pixel;
		gtk_signal_emit(GTK_OBJECT(tkxt), signals[BG_COLOR_CHANGED],
				tkxt->last_bg_color);
	}

	gtk_text_layout_release_style(tkxt->layout, style);
} /* gnome_canvas_tktext_emit_cursor_event */

void
gnome_canvas_tktext_set_buffer(GnomeCanvasTkText *tkxt,
			       GtkTextBuffer *buffer)
{
	g_return_if_fail(GNOME_IS_CANVAS_TKTEXT(tkxt));
	g_return_if_fail(buffer == NULL || GTK_IS_TEXT_VIEW_BUFFER(buffer));

	if (tkxt->buffer == buffer)
		return;

	if (tkxt->buffer != NULL) {
		gtk_signal_disconnect_by_func(GTK_OBJECT(tkxt->buffer),
					      GTK_SIGNAL_FUNC(changed_handler),
					      (gpointer)tkxt);
		gtk_object_unref(GTK_OBJECT(tkxt->buffer));
		tkxt->dnd_mark = NULL;
	}

	tkxt->buffer = buffer;

	if (buffer != NULL) {
		GtkTextIter start;

		gtk_object_ref(GTK_OBJECT(buffer));
		gtk_object_sink(GTK_OBJECT(buffer));

		gnome_canvas_tktext_ensure_layout(tkxt);

		if (tkxt->layout)
			gtk_text_layout_set_buffer(tkxt->layout, buffer);

		gtk_text_buffer_get_iter_at_char(tkxt->buffer, &start, 0);

		tkxt->dnd_mark = gtk_text_buffer_create_mark(tkxt->buffer,
							     "__drag_target",
							     &start, FALSE);
		gtk_signal_connect(GTK_OBJECT(buffer), "changed",
				   GTK_SIGNAL_FUNC(changed_handler),
				   (gpointer)tkxt);
	}

	gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(tkxt));

	{
		GtkTextIter insert;
		/* Force the item to emit font/colour events */
		gtk_text_buffer_get_iter_at_mark(tkxt->buffer,
						 &insert, "insert");
		gnome_canvas_tktext_emit_cursor_event(tkxt, &insert, TRUE);
	}
} /* gnome_canvas_tktext_set_buffer */

GnomeCanvasItem *
gnome_canvas_tktext_new_with_buffer(GtkTextBuffer *buffer)
{
	GnomeCanvasTkText *tkxt;

	tkxt = (GnomeCanvasTkText *)gnome_canvas_tktext_new();

	gnome_canvas_tktext_set_buffer(tkxt, buffer);

	return GNOME_CANVAS_ITEM(tkxt);
} /* gnome_canvas_tktext_new_with_buffer */

GtkTextBuffer *
gnome_canvas_tktext_get_buffer(GnomeCanvasTkText *tkxt)
{
	g_return_val_if_fail(GNOME_IS_CANVAS_TKTEXT(tkxt), NULL);

	return tkxt->buffer;
} /* gnome_canvas_tktext_get_buffer */

void
gnome_canvas_tktext_get_iter_at_pixel(GnomeCanvasTkText *tkxt,
				      GtkTextIter *iter,
				      gint x, gint y)
{
	GnomeCanvasItem *item;
	gint nx, ny;

	g_return_if_fail(GNOME_IS_CANVAS_TKTEXT(tkxt));
	g_return_if_fail(iter != NULL);
	g_return_if_fail(tkxt->layout != NULL);

	item = GNOME_CANVAS_ITEM(tkxt);

	nx = (int)(((double)x + tkxt->xofs - item->x1) /
		   item->canvas->pixels_per_unit);
	ny = (int)(((double)y + tkxt->yofs - item->y1) /
		   item->canvas->pixels_per_unit);

	gtk_text_layout_get_iter_at_pixel(tkxt->layout, iter,
					  nx, ny);
} /* gnome_canvas_tktext_get_iter_at_pixel */

static gboolean
clamp_iter_onscreen(GnomeCanvasTkText *tkxt, GtkTextIter *iter)
{
	GtkTextIter start, end;
	GnomeCanvasItem *item;

	item = GNOME_CANVAS_ITEM(tkxt);

	gnome_canvas_tktext_get_iter_at_pixel(tkxt, &start, 0, 0);
	/* FIXME: allocation? */
	gnome_canvas_tktext_get_iter_at_pixel(tkxt, &end,
					      item->x2 - item->x1,
					      item->y2 - item->y1);

	if (gtk_text_iter_compare(iter, &start) < 0) {
		*iter = start;
		return TRUE;
	} else if (gtk_text_iter_compare(iter, &end) > 0) {
		*iter = end;
		return TRUE;
	} else
		return FALSE;
} /* clamp_iter_onscreen */

gboolean
gnome_canvas_tktext_move_mark_onscreen(GnomeCanvasTkText *tkxt,
				       const gchar *mark_name)
{
	GtkTextIter mark;

	g_return_val_if_fail(GNOME_IS_CANVAS_TKTEXT(tkxt), FALSE);
	g_return_val_if_fail(mark_name != NULL, FALSE);

	if (!gtk_text_buffer_get_iter_at_mark(tkxt->buffer, &mark, mark_name))
		return FALSE;

	if (clamp_iter_onscreen(tkxt, &mark)) {
		gtk_text_buffer_move_mark(tkxt->buffer, mark_name, &mark);
		return TRUE;
	} else
		return FALSE;
} /* gnome_canvas_tktext_move_mark_onscreen */

gboolean
gnome_canvas_tktext_place_cursor_onscreen(GnomeCanvasTkText *tkxt)
{
	GtkTextIter insert;

	g_return_val_if_fail(GNOME_IS_CANVAS_TKTEXT(tkxt), FALSE);

	gtk_text_buffer_get_iter_at_mark(tkxt->buffer, &insert, "insert");

	if (clamp_iter_onscreen(tkxt, &insert)) {
		gtk_text_buffer_place_cursor(tkxt->buffer, &insert);
		gnome_canvas_tktext_emit_cursor_event(tkxt, &insert, FALSE);
		return TRUE;
	} else
		return FALSE;
} /* gnome_canvas_tktext_place_cursor_onscreen */

static void
redraw_world_rect(GnomeCanvas *canvas, double x1, double y1, double x2,
		  double y2)
{
	double w2c[6];
	ArtPoint w1, c1, w2, c2;

	gnome_canvas_w2c_affine (canvas, w2c);

	w1.x = x1;
	w1.y = y1;
	w2.x = x2;
	w2.y = y2;

	art_affine_point (&c1, &w1, w2c);
	art_affine_point (&c2, &w2, w2c);

	/* Do we need fudging here? */

	gnome_canvas_request_redraw (canvas,
				     (int) floor (c1.x + 0.5),
				     (int) floor (c1.y + 0.5),
				     (int) floor (c2.x + 0.5) + 1,
				     (int) floor (c2.y + 0.5) + 1);
} /* redraw_world_rect */

static void
gnome_canvas_tktext_destroy_layout(GnomeCanvasTkText *tkxt)
{
	if (tkxt->layout) {
		gnome_canvas_tktext_end_selection_drag(tkxt, NULL);

		if (tkxt->need_repaint_handler != 0)
			gtk_signal_disconnect(GTK_OBJECT(tkxt->layout),
					      tkxt->need_repaint_handler);
		tkxt->need_repaint_handler = 0;
		gtk_object_unref(GTK_OBJECT(tkxt->layout));
		tkxt->layout = NULL;
	}
} /* gnome_canvas_tktext_destroy_layout */

/* Destroy handler for the text item */
static void
gnome_canvas_tktext_destroy (GtkObject *object)
{
	GnomeCanvasTkText *item;
	GnomeCanvasItem *ci;

	g_return_if_fail(object);
	g_return_if_fail(GNOME_IS_CANVAS_TKTEXT(object));

	item = GNOME_CANVAS_TKTEXT (object);
	ci = GNOME_CANVAS_ITEM (object);

	/* Unhook the blink timeout handler */
	gnome_canvas_tktext_stop_cursor_blink(item);

	/* Redraw last known area */
	if (GNOME_CANVAS_ITEM(item)->canvas->aa)
		; /* FIXME */
	else
		redraw_world_rect (ci->canvas, ci->x1, ci->y1, ci->x2, ci->y2);

	gnome_canvas_tktext_destroy_layout(item);

	if (item->buffer)
		gtk_object_unref(GTK_OBJECT(item->buffer));

	if (GTK_OBJECT_CLASS(parent_class)->destroy)
		(* GTK_OBJECT_CLASS(parent_class)->destroy)(object);
} /* gnome_canvas_tktext_destroy */

static void
gnome_canvas_tktext_finalize (GtkObject *object)
{
	GnomeCanvasTkText *tkxt;

	tkxt = GNOME_CANVAS_TKTEXT(object);

	if (GTK_OBJECT_CLASS(parent_class)->finalize)
		(* GTK_OBJECT_CLASS(parent_class)->finalize) (object);
} /* gnome_canvas_tktext_finalize */

static gchar *
get_unique_tag(void)
{
	/* There is no such thing as an anonymous tag yet, so we use this
	 * little hack to create unique tags.  Don't forget to g_free it! */
	static guint tagnum = 0;

	return g_strdup_printf("ctag-%d", tagnum++);
}

static gboolean
modify_selection(GnomeCanvasTkText *tkxt, GdkFont *font, GdkColor *fg_color,
		 GdkColor *bg_color)
{
	GtkTextIter start, end;
	GtkTextTag *tag;
	gchar *tagname = get_unique_tag();

	if (!gtk_text_buffer_get_selection_bounds(tkxt->buffer,
						  &start, &end))
		return FALSE;

	tag = gtk_text_buffer_create_tag(tkxt->buffer, tagname);

	if (font)
		gtk_object_set(GTK_OBJECT(tag), "font", font, NULL);
	if (fg_color)
		gtk_object_set(GTK_OBJECT(tag),
			       "foreground_gdk", fg_color, NULL);
	if (bg_color)
		gtk_object_set(GTK_OBJECT(tag),
			       "background_gdk", bg_color, NULL);

	gtk_text_buffer_apply_tag(tkxt->buffer, tagname, &start, &end);
	g_free(tagname);

	return TRUE;
}

/* This function is called for the first keystroke after the font/colors are
 * changed when there is no selection */
static void
apply_new_tag(GnomeCanvasTkText *tkxt)
{
	GtkTextIter *start, end;
	GtkTextTag *tag;
	gchar *tagname = get_unique_tag();

	tag = gtk_text_buffer_create_tag(tkxt->buffer, tagname);

	gtk_text_buffer_get_iter_at_mark(tkxt->buffer, &end, "insert");
	start = gtk_text_iter_copy(&end);
	gtk_text_iter_backward_char(start);
	/* gtk_text_iter_forward_char(&end); */

	if (tkxt->new_font) {
		gtk_object_set(GTK_OBJECT(tag), "font", tkxt->new_font, NULL);
		gdk_font_unref(tkxt->new_font);
		tkxt->new_font = NULL;
	}
	if (tkxt->new_fg_color) {
		gtk_object_set(GTK_OBJECT(tag),
			       "foreground_gdk", tkxt->new_fg_color, NULL);
		gdk_color_free(tkxt->new_fg_color);
		tkxt->new_fg_color = NULL;
	}
	if (tkxt->new_bg_color) {
		gtk_object_set(GTK_OBJECT(tag),
			       "background_gdk", tkxt->new_bg_color, NULL);
		gdk_color_free(tkxt->new_bg_color);
		tkxt->new_bg_color = NULL;
	}

	gtk_text_buffer_apply_tag(tkxt->buffer, tagname, start, &end);
	gtk_text_iter_free(start);
	g_free(tagname);
}

static void
gnome_canvas_tktext_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
	GnomeCanvasTkText *tkxt;
	GnomeCanvasItem *item;

	g_return_if_fail(GNOME_IS_CANVAS_TKTEXT(object));
	tkxt = GNOME_CANVAS_TKTEXT(object);
	item = GNOME_CANVAS_ITEM(tkxt);

	switch (arg_id) {
	case ARG_X:
		tkxt->x = GTK_VALUE_DOUBLE(*arg);

		gnome_canvas_item_request_update(item);
		break;

	case ARG_Y:
		tkxt->y = GTK_VALUE_DOUBLE(*arg);

		gnome_canvas_item_request_update(item);
		break;

#if 0
	case ARG_WIDTH:
		tkxt->width = GTK_VALUE_DOUBLE(*arg);

		gnome_canvas_item_request_update(item);
		break;

	case ARG_HEIGHT:
		tkxt->height = GTK_VALUE_DOUBLE(*arg);

		gnome_canvas_item_request_update(item);
		break;
#endif

	case ARG_FONT_GDK:
	{
		GdkFont *font;

		font = GTK_VALUE_BOXED(*arg);

		if (tkxt->new_font) {
			gdk_font_unref(tkxt->new_font);
			tkxt->new_font = NULL;
		}

		/* modify_selection returns whether a selection exists */
		if (!modify_selection(tkxt, font, NULL, NULL)) {
			gdk_font_ref(font);
			tkxt->new_font = font;
		}

		break;
	}
	case ARG_FG_COLOR_GDK:
	{
		GdkColor *color;

		color = GTK_VALUE_BOXED(*arg);

		if (tkxt->new_fg_color) {
			g_free(tkxt->new_fg_color);
			tkxt->new_fg_color = NULL;
		}

		/* modify_selection returns whether a selection exists */
		if (!modify_selection(tkxt, NULL, color, NULL))
			tkxt->new_fg_color = gdk_color_copy(color);

		break;
	}
	case ARG_BG_COLOR_GDK:
	{
		GdkColor *color;

		color = GTK_VALUE_BOXED(*arg);

		if (tkxt->new_bg_color) {
			g_free(tkxt->new_bg_color);
			tkxt->new_bg_color = NULL;
		}

		/* modify_selection returns whether a selection exists */
		if (!modify_selection(tkxt, NULL, NULL, color))
			tkxt->new_bg_color = gdk_color_copy(color);

		break;
	}
	case ARG_HEIGHT_LINES:
		break;

	case ARG_WIDTH_COLUMNS:
		break;

	case ARG_PIXELS_ABOVE_LINES:
		break;

	case ARG_PIXELS_BELOW_LINES:
		break;

	case ARG_PIXELS_INSIDE_WRAP:
		break;

	case ARG_EDITABLE:
		tkxt->editable = GTK_VALUE_INT(*arg);

		if (tkxt->editable)
			gnome_canvas_tktext_focus_in_event(item, NULL);
		else
			gnome_canvas_tktext_focus_out_event(item, NULL);
		gnome_canvas_item_request_update(item);
		break;

	case ARG_WRAP_MODE:
		break;

	default:
		g_assert_not_reached();
		break;
	}
} /* gnome_canvas_tktext_set_arg */

static void
gnome_canvas_tktext_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
	GnomeCanvasTkText *tkxt;

	tkxt = GNOME_CANVAS_TKTEXT(object);

	switch (arg_id) {
	case ARG_NUM_LINES:
		GTK_VALUE_INT(*arg) =
			gtk_text_btree_line_count(tkxt->buffer->tree);
		break;
	case ARG_HEIGHT_LINES:
		break;

	case ARG_WIDTH_COLUMNS:
		break;

	case ARG_PIXELS_ABOVE_LINES:
		break;

	case ARG_PIXELS_BELOW_LINES:
		break;

	case ARG_PIXELS_INSIDE_WRAP:
		break;

	case ARG_EDITABLE:
		break;

	case ARG_WRAP_MODE:
		break;

	default:
		arg->type = GTK_TYPE_INVALID;
		break;
	}
} /* gnome_canvas_tktext_get_arg */

gchar *
gnome_canvas_tktext_get_text_at_line(GnomeCanvasTkText *tkxt, gint line)
{
	g_return_val_if_fail(tkxt, NULL);

	return gtk_text_buffer_get_text_from_line(tkxt->buffer, line,
						  0, -1, FALSE);
}

GSList *
gnome_canvas_tktext_get_tags_at_line(GnomeCanvasTkText *tkxt, gint line)
{
	GtkTextIter iter;
	GSList *tags;

	g_return_val_if_fail(tkxt, NULL);

	gtk_text_buffer_get_iter_at_line(tkxt->buffer, &iter, line);
	tags = gtk_text_iter_get_toggled_tags(&iter, TRUE);
	while (!gtk_text_iter_ends_line(&iter)) {
		gtk_text_iter_forward_indexable_segment(&iter);
		g_slist_concat(tags, gtk_text_iter_get_toggled_tags
			       (&iter, TRUE));
	}
	return tags;
}

static void
gnome_canvas_tktext_realize (GnomeCanvasItem *item)
{
	GnomeCanvasTkText *tkxt;

	tkxt = GNOME_CANVAS_TKTEXT(item);

	if (GNOME_CANVAS_ITEM_CLASS(parent_class)->realize)
		(* GNOME_CANVAS_ITEM_CLASS(parent_class)->realize) (item);

	tkxt->gc = gdk_gc_new (item->canvas->layout.bin_window);

	gnome_canvas_tktext_ensure_layout(tkxt);
} /* gnome_canvas_tktext_realize */

static void
gnome_canvas_tktext_unrealize (GnomeCanvasItem *item)
{
	GnomeCanvasTkText *tkxt;

	tkxt = GNOME_CANVAS_TKTEXT(item);

	gdk_gc_unref(tkxt->gc);
	tkxt->gc = NULL;

	gnome_canvas_tktext_destroy_layout(tkxt);

	if (GNOME_CANVAS_ITEM_CLASS(parent_class)->unrealize)
		(* GNOME_CANVAS_ITEM_CLASS(parent_class)->unrealize) (item);
} /* gnome_canvas_tktext_unrealize */


/*
 * Events
 */

#if 0
static gboolean
get_event_coordinates(GdkEvent *event, gint *x, gint *y)
{
	if (event)
		switch (event->type) {
		case GDK_MOTION_NOTIFY:
			*x = event->motion.x;
			*y = event->motion.y;
			return TRUE;
			break;
        
		case GDK_BUTTON_PRESS:
		case GDK_2BUTTON_PRESS:
		case GDK_3BUTTON_PRESS:
		case GDK_BUTTON_RELEASE:
			*x = event->button.x;
			*y = event->button.y;
			return TRUE;
			break;
        
		case GDK_KEY_PRESS:
		case GDK_KEY_RELEASE:
		case GDK_ENTER_NOTIFY:
		case GDK_LEAVE_NOTIFY:
		case GDK_PROPERTY_NOTIFY:
		case GDK_SELECTION_CLEAR:
		case GDK_SELECTION_REQUEST:
		case GDK_SELECTION_NOTIFY:
		case GDK_PROXIMITY_IN:
		case GDK_PROXIMITY_OUT:
		case GDK_DRAG_ENTER:
		case GDK_DRAG_LEAVE:
		case GDK_DRAG_MOTION:
		case GDK_DRAG_STATUS:
		case GDK_DROP_START:
		case GDK_DROP_FINISHED:
		default:
			return FALSE;
			break;
		}

	return FALSE;
} /* get_event_coordinates */
#endif

static gint
gnome_canvas_tktext_key_press_event(GnomeCanvasItem *item, GdkEventKey *event)
{
	GnomeCanvasTkText *tkxt;
	gboolean extend = FALSE;
	gint relevant_mask, less_relevant_mask;

	g_return_val_if_fail(item, TRUE);
	g_return_val_if_fail(event, TRUE);

	tkxt = GNOME_CANVAS_TKTEXT(item);

	if (tkxt->layout == NULL ||
	    tkxt->buffer == NULL)
		return FALSE;

#if 0
	if (event->length <= 0)
		return FALSE;
#endif

	if ((event->state & (GDK_MOD2_MASK | GDK_MOD3_MASK | GDK_MOD4_MASK |
			     GDK_MOD5_MASK)))
		/* we know nothing about these modifier bits. */
		return FALSE;

	less_relevant_mask = event->state & (GDK_CONTROL_MASK | GDK_MOD1_MASK);
	relevant_mask = event->state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK |
					GDK_MOD1_MASK);

	if (event->state & GDK_SHIFT_MASK) {
		event->state &= ~GDK_SHIFT_MASK;
		extend = TRUE;
	}

	switch (event->keyval) {
	case GDK_Return:
		gtk_text_buffer_delete_selection(tkxt->buffer);
		gtk_text_buffer_insert_at_cursor(tkxt->buffer, "\n", 1);
		return TRUE;

		/* Moving around. */
	case GDK_Right:
		if (!less_relevant_mask) {
			gnome_canvas_tktext_move_insert(tkxt,
							GTK_TEXT_MOVEMENT_CHAR,
							1, extend);
			return TRUE;
		} else if (less_relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_move_insert(tkxt,
							GTK_TEXT_MOVEMENT_WORD,
							1, extend);
			return TRUE;
		}
		break;
	case GDK_Left:
		if (!less_relevant_mask) {
			gnome_canvas_tktext_move_insert(tkxt,
							GTK_TEXT_MOVEMENT_CHAR,
							-1, extend);
			return TRUE;
		} else if (less_relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_move_insert(tkxt,
							GTK_TEXT_MOVEMENT_WORD,
							-1, extend);
			return TRUE;
		}
		break;
	case GDK_f:
		if (less_relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_move_insert(tkxt,
							GTK_TEXT_MOVEMENT_CHAR,
							1, extend);
			return TRUE;
		} else if (less_relevant_mask == GDK_MOD1_MASK) {
			gnome_canvas_tktext_move_insert(tkxt,
							GTK_TEXT_MOVEMENT_WORD,
							1, extend);
			return TRUE;
		}
		break;
	case GDK_b:
		if (less_relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_move_insert(tkxt,
							GTK_TEXT_MOVEMENT_CHAR,
							-1, extend);
			return TRUE;
		} else if (less_relevant_mask == GDK_MOD1_MASK) {
			gnome_canvas_tktext_move_insert(tkxt,
							GTK_TEXT_MOVEMENT_WORD,
							-1, extend);
			return TRUE;
		}
		break;
	case GDK_Up:
		if (!less_relevant_mask) {
			gnome_canvas_tktext_move_insert(tkxt, GTK_TEXT_MOVEMENT_PARAGRAPH,
							-1, extend);
			return TRUE;
		}
		break;
	case GDK_Down:
		if (!less_relevant_mask) {
			gnome_canvas_tktext_move_insert(tkxt, GTK_TEXT_MOVEMENT_PARAGRAPH,
							1, extend);
			return TRUE;
		}
		break;
	case GDK_p:
		if (less_relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_move_insert(tkxt, GTK_TEXT_MOVEMENT_PARAGRAPH,
							-1, extend);
			return TRUE;
		}
		break;
	case GDK_n:
		if (less_relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_move_insert(tkxt, GTK_TEXT_MOVEMENT_PARAGRAPH,
							1, extend);
			return TRUE;
		}
		break;
	case GDK_a:
		if (less_relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_move_insert(tkxt, GTK_TEXT_MOVEMENT_PARAGRAPH_ENDS,
							-1, extend);
			return TRUE;
		}
		break;
	case GDK_e:
		if (less_relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_move_insert(tkxt, GTK_TEXT_MOVEMENT_PARAGRAPH_ENDS,
							1, extend);
			return TRUE;
		}
		break;
	case GDK_Home:
		if (!less_relevant_mask) {
			gnome_canvas_tktext_move_insert(tkxt, GTK_TEXT_MOVEMENT_BUFFER_ENDS,
							-1, extend);
			return TRUE;
		}
		break;
	case GDK_End:
		if (!less_relevant_mask) {
			gnome_canvas_tktext_move_insert(tkxt, GTK_TEXT_MOVEMENT_BUFFER_ENDS,
							1, extend);
			return TRUE;
		}
		break;

		/* Setting the cut/paste/copy anchor and deleting text */
	case GDK_space:
		if (extend)
			break;

		if (relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_set_anchor(tkxt);
			return TRUE;
		} else if (relevant_mask == GDK_MOD1_MASK) {
			gnome_canvas_tktext_delete_text(tkxt,
							GTK_TEXT_DELETE_WHITESPACE_LEAVE_ONE,
							1);
			return TRUE;
		}
		break;

		/* Deleting text */
	case GDK_Delete:
		if (extend)
			break;

		if (!relevant_mask) {
			gnome_canvas_tktext_delete_text(tkxt,
							GTK_TEXT_DELETE_CHAR,
							1);
			return TRUE;
		} else if (relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_delete_text(tkxt,
							GTK_TEXT_DELETE_HALF_WORD,
							1);
			return TRUE;
		}
		break;
	case GDK_BackSpace:
		if (extend)
			break;

		if (!relevant_mask) {
			gnome_canvas_tktext_delete_text(tkxt,
							GTK_TEXT_DELETE_CHAR,
							-1);
			return TRUE;
		} else if (relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_delete_text(tkxt,
							GTK_TEXT_DELETE_HALF_WORD,
							-1);
			return TRUE;
		}
		break;
	case GDK_k:
		if (extend)
			break;

		if (relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_delete_text(tkxt,
							GTK_TEXT_DELETE_HALF_PARAGRAPH,
							1);
			return TRUE;
		}
		break;
	case GDK_u:
		if (extend)
			break;

		if (relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_delete_text(tkxt,
							GTK_TEXT_DELETE_WHOLE_PARAGRAPH,
							1);
			return TRUE;
		}
		break;
	case GDK_backslash:
		if (extend)
			break;

		if (relevant_mask == GDK_MOD1_MASK) {
			gnome_canvas_tktext_delete_text(tkxt,
							GTK_TEXT_DELETE_WHITESPACE,
							1);
			return TRUE;
		}
		break;

		/* Cut/copy/paste */
	case GDK_x:
		if (extend)
			break;

		if (relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_cut_text(tkxt);
			return TRUE;
		}
		break;

	case GDK_w:
		if (extend)
			break;

		if (relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_cut_text(tkxt);
			return TRUE;
		}
		break;
		
	case GDK_c:
		if (extend)
			break;

		if (relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_copy_text(tkxt);
			return TRUE;
		}
		break;

	case GDK_y:
		if (extend)
			break;

		if (relevant_mask == GDK_CONTROL_MASK) {
			gnome_canvas_tktext_paste_text(tkxt);
			return TRUE;
		}
		break;

		/* Overwrite */
	case GDK_Insert:
		if (!relevant_mask) {
			gnome_canvas_tktext_toggle_overwrite(tkxt);
			return TRUE;
		}
		break;

	default:
	}

	if (event->length <= 0 || less_relevant_mask)
		return FALSE;

	if (tkxt->overwrite_mode)
		gnome_canvas_tktext_delete_text(tkxt, GTK_TEXT_DELETE_CHAR, 1);
	else
		gtk_text_buffer_delete_selection(tkxt->buffer);
	gtk_text_buffer_insert_at_cursor(tkxt->buffer,
					 event->string,
					 event->length);
	apply_new_tag(tkxt);

	return TRUE;
} /* gnome_canvas_tktext_key_press_event */

static gint
gnome_canvas_tktext_key_release_event(GnomeCanvasItem *item,GdkEventKey *event)
{
	return FALSE;
} /* gnome_canvas_tktext_key_release_event */

static gint
gnome_canvas_tktext_button_press_event(GnomeCanvasItem *item,
				       GdkEventButton *event)
{
	GnomeCanvasTkText *tkxt;

	g_return_val_if_fail(item, TRUE);
	g_return_val_if_fail(event, TRUE);

	tkxt = GNOME_CANVAS_TKTEXT(item);

	/* debug hack */
	if (event->button == 3 && (event->state & GDK_CONTROL_MASK) != 0)
		gtk_text_buffer_spew(tkxt->buffer);
	else if (event->button == 3)
		gtk_text_layout_spew(tkxt->layout);

	if (event->button == 1) {
		/* If we're in the selection, start a drag copy/move of
		 * the selection; otherwise, start creating a new
		 * selection. */
		GtkTextIter iter;
		/* GtkTextIter start, end; */
		gint x, y;

		gnome_canvas_w2c(item->canvas, (gdouble)event->x,
				 (gdouble)event->y, &x, &y);

		gnome_canvas_tktext_get_iter_at_pixel(tkxt,
						      &iter, x, y);

#ifdef TKXT_DND
		if (gtk_text_buffer_get_selection_bounds(tkxt->buffer,
							 &start, &end)
		    && gtk_text_iter_in_region(&iter, &start, &end)) {
			gnome_canvas_tktext_start_selection_dnd(tkxt, &iter,
								event);
		} else
#endif
		{
			gnome_canvas_tktext_start_selection_drag(tkxt, &iter,
								 event);
		}
		return TRUE;
	} else if (event->button == 2) {
		GtkTextIter iter;
		gint x, y;

		gnome_canvas_w2c(item->canvas, (gdouble)event->x,
				 (gdouble)event->y, &x, &y);

		gnome_canvas_tktext_get_iter_at_pixel(tkxt, &iter, x, y);

		gtk_text_buffer_paste_primary_selection(tkxt->buffer,
							&iter,
							event->time);
		return TRUE;
	} else if (event->button == 3) {
		if (gnome_canvas_tktext_end_selection_drag(tkxt,event))
			return TRUE;
		else
			return FALSE;
	}

	return FALSE;
} /* gnome_canvas_tktext_button_press_event */

static gint
gnome_canvas_tktext_button_release_event(GnomeCanvasItem *item,
					 GdkEventButton *event)
{
	GnomeCanvasTkText *tkxt;

	g_return_val_if_fail(item, TRUE);
	g_return_val_if_fail(event, TRUE);

	tkxt = GNOME_CANVAS_TKTEXT(item);

	if (event->button == 1) {
		gnome_canvas_tktext_end_selection_drag
			(GNOME_CANVAS_TKTEXT(item), event);
		return TRUE;
	}

	return FALSE;
} /* gnome_canvas_tktext_button_release_event */


/*
 * Blink!
 */

static gint
blink_cb(gpointer data)
{
	GnomeCanvasTkText *tkxt;
	GtkTextMark *insert;

	g_return_val_if_fail(data, FALSE);

	tkxt = GNOME_CANVAS_TKTEXT(data);

	insert = gtk_text_buffer_get_mark(tkxt->buffer,
					  "insert");

	gtk_text_mark_set_visible(insert, !gtk_text_mark_is_visible(insert));

	return TRUE;
} /* blink_cb */

static void
gnome_canvas_tktext_start_cursor_blink(GnomeCanvasTkText *tkxt)
{
	g_return_if_fail(tkxt);

	if (tkxt->blink_timeout != 0)
		return;

	tkxt->blink_timeout = gtk_timeout_add(500, blink_cb, tkxt);
} /* gnome_canvas_tktext_start_cursor_blink */

static void
gnome_canvas_tktext_stop_cursor_blink(GnomeCanvasTkText *tkxt)
{
	g_return_if_fail(tkxt);

	if (tkxt->blink_timeout == 0)
		return;

	gtk_timeout_remove(tkxt->blink_timeout);
	tkxt->blink_timeout = 0;
} /* gnome_canvas_tktext_stop_cursor_blink */


/*
 * Event handling
 */

static gint
gnome_canvas_tktext_enter_event(GnomeCanvasItem *item, GdkEventCrossing *event)
{
	GdkCursor *cursor;

	g_return_val_if_fail(item, TRUE);

	/* I-beam cursor */
	cursor = gdk_cursor_new(GDK_XTERM);
	gdk_window_set_cursor(item->canvas->layout.bin_window, cursor);
	gdk_cursor_destroy(cursor);

	return FALSE;
} /* gnome_canvas_tktext_enter_event */

static gint
gnome_canvas_tktext_leave_event(GnomeCanvasItem *item, GdkEventCrossing *event)
{
	g_return_val_if_fail(item, TRUE);

	/* Remove I-beam cursor */
	gdk_window_set_cursor(item->canvas->layout.bin_window, NULL);

	return FALSE;
} /* gnome_canvas_tktext_leave_event */

static gint
gnome_canvas_tktext_focus_in_event (GnomeCanvasItem *item,GdkEventFocus *event)
{
	GtkTextMark *insert;
	GnomeCanvasTkText *tkxt;

	g_return_val_if_fail(item, TRUE);

	tkxt = GNOME_CANVAS_TKTEXT(item);

	insert = gtk_text_buffer_get_mark(tkxt->buffer, "insert");
	gtk_text_mark_set_visible(insert, TRUE);

	gnome_canvas_tktext_start_cursor_blink(tkxt);

	return FALSE;
} /* gnome_canvas_tktext_focus_in_event */

static gint
gnome_canvas_tktext_focus_out_event(GnomeCanvasItem *item,GdkEventFocus *event)
{
	GtkTextMark *insert;
	GnomeCanvasTkText *tkxt;

	g_return_val_if_fail(item, TRUE);

	tkxt = GNOME_CANVAS_TKTEXT(item);

	/* GTK_WIDGET_UNSET_FLAGS (widget, GTK_HAS_FOCUS); */

	insert = gtk_text_buffer_get_mark(tkxt->buffer, "insert");
	gtk_text_mark_set_visible(insert, FALSE);

	gnome_canvas_tktext_stop_cursor_blink(tkxt);

	return FALSE;
} /* gnome_canvas_tktext_focus_out_event */

static gint
gnome_canvas_tktext_event(GnomeCanvasItem *item, GdkEvent *event)
{
	GnomeCanvasTkText *tkxt;

	g_return_val_if_fail(item, TRUE);

	tkxt = GNOME_CANVAS_TKTEXT(item);

	if (tkxt->layout == NULL ||
	    tkxt->buffer == NULL)
		return FALSE;

	/* FIXME eventually we really want to synthesize enter/leave
	   events here as the canvas does for canvas items */
	switch (event->type) {
	case GDK_ENTER_NOTIFY:
		return gnome_canvas_tktext_enter_event(item, (GdkEventCrossing *)event);
	case GDK_LEAVE_NOTIFY:
		return gnome_canvas_tktext_leave_event(item, (GdkEventCrossing *)event);
	case GDK_FOCUS_CHANGE:
		if (((GdkEventFocus *)event)->in)
			return gnome_canvas_tktext_focus_in_event(item, (GdkEventFocus *)event);
		else
			return gnome_canvas_tktext_focus_out_event(item, (GdkEventFocus *)event);
	case GDK_MOTION_NOTIFY:
	{
#if 0
		gint x = 0, y = 0;
#endif

		if (tkxt->selection_drag_handler)
			return (* tkxt->selection_drag_handler)(tkxt, (GdkEventMotion *)event);
		break;
#if 0
		if (get_event_coordinates(event, &x, &y)) {
			GtkTextIter iter;
			gint retval = FALSE;
			GSList *tags, *tmp;

			x += tkxt->xofs;
			y += tkxt->yofs;

			/* FIXME this is slow and we do it twice per event.
			   My favorite solution is to have GtkTextLayout cache
			   the last couple lookups. */
			gnome_canvas_tktext_get_iter_at_pixel(tkxt->layout,
							  &iter,
							  x, y);

			tags = gtk_text_buffer_get_tags(tkxt->buffer, &iter);

			tmp = tags;
			while (tmp != NULL) {
				GtkTextTag *tag = tmp->data;

				if (gtk_text_tag_event(tag, GTK_OBJECT(item), event,
						       &iter)) {
					retval = TRUE;
					break;
				}
				tmp = g_slist_next(tmp);
			}
			g_slist_free(tags);

			return retval;
		}
		break;
#endif
	}
	case GDK_KEY_PRESS:
		return gnome_canvas_tktext_key_press_event(item,(GdkEventKey *)event);
	case GDK_KEY_RELEASE:
		return gnome_canvas_tktext_key_release_event(item, (GdkEventKey *)event);
	case GDK_BUTTON_PRESS:
		return gnome_canvas_tktext_button_press_event(item, (GdkEventButton *)event);
	case GDK_BUTTON_RELEASE:
		return gnome_canvas_tktext_button_release_event(item, (GdkEventButton *)event);
	default:
	}

	return FALSE;
} /* gnome_canvas_tktext_event */

/* X and Y are the upper left hand corner of the drawable with respect to the
 * canvas coords (0,0).  Who knew!? */
static void
gnome_canvas_tktext_draw(GnomeCanvasItem *item, GdkDrawable *drawable,
			 int x, int y, int width, int height)
{
	GnomeCanvasTkText *tkxt;
	gint x_offset, y_offset, scale_wid, scale_ht;
	GdkRectangle area;

	GdkPixmap *pixmap;
	GdkPixbuf *pb, *new_pb;
	GdkColor fg;
	char * pixels, r, g, b;

	tkxt = GNOME_CANVAS_TKTEXT(item);

	g_return_if_fail(tkxt->layout != NULL);
	g_return_if_fail(tkxt->xofs >= 0);
	g_return_if_fail(tkxt->yofs >= 0);

#if 0
	printf("painting %d,%d  %d x %d\n", x, y, width, height);
#endif

	x_offset = x - tkxt->cx;
	y_offset = y - tkxt->cy;

	if (x < tkxt->cx)
		area.x = 0;
	else
		area.x = x_offset;
	if (y < tkxt->cy)
		area.y = 0;
	else
		area.y = y_offset;
	area.width = (x + width) - tkxt->cx;
	area.height = (y + height) - tkxt->cy;
	if (area.width > width)
		area.width = width;
	if (area.height > height)
		area.height = height;
	if (area.width > (item->x2 - item->x1))
		area.width = item->x2 - item->x1;
	if (area.height > (item->y2 - item->y1))
		area.height = item->y2 - item->y1;

	if (area.width <= 0 || area.height <= 0) {
		printf("width (%d) or height (%d) is <= 0!\n", area.width,
		       area.height);
		printf("x: %d, y: %d, tkxt->cx: %d, tkxt->cy: %d\n", x, y,
		       tkxt->cx, tkxt->cy);
		printf("x_offset: %d, y_offset: %d, area.x: %d, area.y: %d\n",
		       x_offset, y_offset, area.x, area.y);
		return;
	}

#if 0
	if (tkxt->clip) {
		screen.x = tkxt->clip_cx;
		screen.y = tkxt->clip_cy;
		screen.width = tkxt->clip_cwidth;
		screen.height = tkxt->clip_cheight;

		if (!gdk_rectangle_intersect(&area, &screen, &repaint_area)) {
			printf("repaint outside of clip area, returning\n");
			return;
		}
	} else
	{
		screen.x = tkxt->cx;
		screen.y = tkxt->cy;
		screen.width = item->x2 - item->y1;
		screen.height = item->y2 - item->y1;

		repaint_area.x = area.x;
		repaint_area.y = area.y;
		repaint_area.width = area.width;
		repaint_area.height = area.height;
	}
#endif

#if 0
	printf("area  : x=%d, y=%d, width=%d, height=%d\n", area.x, area.y,
	       area.width, area.height);
#endif

	/* Because doing the AA canvas item properly would take more time than
	 * I have right now, I have chosen to hack proper zooming by creating
	 * a pixmap, rendering to it, transforming it, and painting it back to
	 * the canvas.  Fear.
	 *
	 * Additional ugly: because I'm not guaranteed that my background
	 * colour will stay the same when I convert from drawable to pixbuf,
	 * I make the drawable and pixbuf one extra pixel wide and shift the
	 * render operation over by one pixel--I'm guaranteed that the leftmost
	 * column will never be rendered over and will thus remain magenta.
	 * Then I can check the colour values of pixel 0 and globally replace
	 * that colour with a pure alpha channel. */
	if (item->canvas->pixels_per_unit == 1.0) {
		/* Mega-optimisation */
		gtk_text_layout_draw(tkxt->layout, GTK_WIDGET(item->canvas),
				     drawable, x_offset, y_offset,
				     area.x, area.y,
				     area.width, area.height);
		return;
	}

#define EXTRA 1

	pixmap = gdk_pixmap_new(item->canvas->layout.bin_window,
				area.width + EXTRA, area.height, -1);

	{
		GdkGC *gc = gdk_gc_new(item->canvas->layout.bin_window);

		gdk_color_parse("#FF02FF", &fg);
		gdk_colormap_alloc_color(gtk_widget_get_colormap(GTK_WIDGET(item->canvas)),
					 &fg, FALSE, TRUE);
		gdk_gc_set_foreground(gc, &fg);
		gdk_draw_rectangle(pixmap, gc, TRUE, 0, 0, -1, -1);
		gdk_gc_unref(gc);
	}

	gtk_text_layout_draw(tkxt->layout, GTK_WIDGET(item->canvas), pixmap,
			     0 + EXTRA, 0,
			     area.x, area.y,
			     area.width, area.height);

#if 1
	/* Now, convert it to a GdkPixbuf */
	pb = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8, area.width + EXTRA,
			    area.height);
	gdk_pixbuf_get_from_drawable(pb, pixmap,
				     gtk_widget_get_colormap(GTK_WIDGET(item->canvas)),
				     0, 0, 0, 0,
				     area.width + EXTRA, area.height);

	/* Find our target colour */
	pixels = gdk_pixbuf_get_pixels(pb);
	r = pixels[0];
	g = pixels[1];
	b = pixels[2];

	/* Make sure we have at least a pixel to scale to. */
	if((scale_wid = area.width * item->canvas->pixels_per_unit) <= 0)
		scale_wid = 1;
	if((scale_ht = area.height * item->canvas->pixels_per_unit) <= 0)
		scale_ht = 1;
	
	/* Scale the shit out of it */
	new_pb = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8, 
				scale_wid, scale_ht);
	gdk_pixbuf_scale(pb, new_pb,
			 0, 0,
			 scale_wid, scale_ht,
			 0.0, 0.0,
			 item->canvas->pixels_per_unit,
			 item->canvas->pixels_per_unit,
			 GDK_INTERP_NEAREST);

	gdk_pixbuf_unref(pb);
	pb = gdk_pixbuf_add_alpha(new_pb, TRUE, r, g, b);
	gdk_pixbuf_unref(new_pb);
	gdk_pixbuf_render_to_drawable_alpha(pb, drawable,
					    0, 0, -x_offset, -y_offset,
			 		    scale_wid, scale_ht,
					    GDK_PIXBUF_ALPHA_BILEVEL,
					    255,
					    GDK_RGB_DITHER_NORMAL, 0, 0);
	gdk_pixbuf_unref(pb);
#else
	/* DEBUG: ...or just draw it. */
	gdk_draw_pixmap(drawable, tkxt->gc, pixmap,
			0, 0, -x_offset, -y_offset,
			area.width, area.height);
	printf("pixmap  : x=%d, y=%d, width=%d, height=%d\n",
	       x, y, width, height);
	printf("boundbox: x1=%d, y1=%d, x2=%d, y2=%d\n",
	       (int)item->x1, (int)item->y1, (int)item->x2, (int)item->y2);
	printf("x_offset=%d, y_offset=%d ", x_offset, y_offset);
	printf(" area.x=%d, area.y=%d, area.width=%d, area.height=%d\n\n",
	       area.x, area.y, area.width, area.height);
#endif

	gdk_pixmap_unref(pixmap);
} /* gnome_canvas_tktext_draw */

/*
 * Key binding handlers
 */


/*
 * Layout utils
 */

static void
gnome_canvas_tktext_ensure_layout(GnomeCanvasTkText *tkxt)
{
	if (tkxt->layout == NULL) {
		GtkTextStyleValues *style;
		GtkWidget *widget;

		tkxt->layout = gtk_text_layout_new();

		gtk_signal_connect(GTK_OBJECT(tkxt->layout), "need_repaint",
			     GTK_SIGNAL_FUNC(gnome_canvas_tktext_need_repaint),
				   tkxt);

		if (tkxt->buffer)
			gtk_text_layout_set_buffer(tkxt->layout, tkxt->buffer);

		style = gtk_text_view_style_values_new();

		widget = GTK_WIDGET(GNOME_CANVAS_ITEM(tkxt)->canvas);
		gtk_widget_ensure_style(widget);

		style->bg_color = widget->style->base[GTK_STATE_NORMAL];
		style->fg_color = widget->style->fg[GTK_STATE_NORMAL];

		style->font = widget->style->font;

		style->pixels_above_lines = 2;
		style->pixels_below_lines = 2;
		style->pixels_inside_wrap = 1;

		style->wrap_mode = GTK_WRAPMODE_NONE;
		style->justify = GTK_JUSTIFY_LEFT;

		gtk_text_layout_set_default_style(tkxt->layout,
						  style);

		gtk_text_view_style_values_unref(style);
	}
} /* gnome_canvas_tktext_ensure_layout */

static void
get_bounds (GnomeCanvasTkText *text, double *px1, double *py1,
	    double *px2, double *py2)
{
	GnomeCanvasItem *item;
	double x, y;
	gint width = 0, height = 0;

	item = GNOME_CANVAS_ITEM (text);

	/* text->{x,y} is the position at the anchor */
	x = text->x;
	y = text->y;

	/* Get canvas pixel coordinates for text position */
	gnome_canvas_item_i2w(item, &x, &y);
	gnome_canvas_w2c(item->canvas, x + text->xofs, y + text->yofs,
			 &text->cx, &text->cy);

	/* Calculate text dimensions */
	if (text->layout) {
		gtk_text_layout_get_size(text->layout, &width, &height);
		width *= item->canvas->pixels_per_unit;
		height *= item->canvas->pixels_per_unit;
	} else {
		*px1 = *px2 = x;
		*py1 = *py2 = y;
		return;
	}

	/* Get canvas pixel coordinates for clip rectangle position */
	gnome_canvas_w2c(item->canvas, x, y, &text->clip_cx, &text->clip_cy);
	if (text->clip) {
		text->clip_cwidth =
			text->clip_width * item->canvas->pixels_per_unit;
		text->clip_cheight =
			text->clip_height * item->canvas->pixels_per_unit;
	} else {
		text->clip_cwidth = width * item->canvas->pixels_per_unit;
		text->clip_cheight = height * item->canvas->pixels_per_unit;
	}

	/* Anchor text */
	switch (text->anchor) {
	case GTK_ANCHOR_NW:
	case GTK_ANCHOR_W:
	case GTK_ANCHOR_SW:
		break;

	case GTK_ANCHOR_N:
	case GTK_ANCHOR_CENTER:
	case GTK_ANCHOR_S:
		text->cx -= width / 2;
		text->clip_cx -= text->clip_cwidth / 2;
		break;

	case GTK_ANCHOR_NE:
	case GTK_ANCHOR_E:
	case GTK_ANCHOR_SE:
		text->cx -= width;
		text->clip_cx -= text->clip_cwidth;
		break;
	}

	switch (text->anchor) {
	case GTK_ANCHOR_NW:
	case GTK_ANCHOR_N:
	case GTK_ANCHOR_NE:
		break;

	case GTK_ANCHOR_W:
	case GTK_ANCHOR_CENTER:
	case GTK_ANCHOR_E:
		text->cy -= height / 2;
		text->clip_cy -= text->clip_cheight / 2;
		break;

	case GTK_ANCHOR_SW:
	case GTK_ANCHOR_S:
	case GTK_ANCHOR_SE:
		text->cy -= height;
		text->clip_cy -= text->clip_cheight;
		break;
	}

	/* Bounds */
	if (text->clip) {
		*px1 = text->clip_cx;
		*py1 = text->clip_cy;
		*px2 = *px1 + text->clip_cwidth;
		*py2 = *py1 + text->clip_cheight;
	} else {
		*px1 = text->cx;
		*py1 = text->cy;
		*px2 = *px1 + width;
		*py2 = *py1 + height;
	}
} /* get_bounds */

#if 0
static void
get_bounds_relative (GnomeCanvasTkText *text, double *px1, double *py1,
		     double *px2, double *py2)
{
        GnomeCanvasItem *item;
        double x, y;
	gint width = 0, height = 0;
        double clip_x, clip_y;

	g_return_if_fail(text);
        item = GNOME_CANVAS_ITEM (text);

        x = text->x;
        y = text->y;

        clip_x = x;
        clip_y = y;

	/* Calculate text dimensions */
	if (text->layout)
		gtk_text_layout_get_size(text->layout, &width, &height);
	else {
		*px1 = *px2 = x;
		*py1 = *py2 = y;
		return;
	}

	/* Anchor text */
	switch (text->anchor) {
	case GTK_ANCHOR_NW:
	case GTK_ANCHOR_W:
	case GTK_ANCHOR_SW:
		break;

	case GTK_ANCHOR_N:
	case GTK_ANCHOR_CENTER:
	case GTK_ANCHOR_S:
		text->cx -= width / 2;
		text->clip_cx -= text->clip_cwidth / 2;
		break;

	case GTK_ANCHOR_NE:
	case GTK_ANCHOR_E:
	case GTK_ANCHOR_SE:
		text->cx -= width;
		text->clip_cx -= text->clip_cwidth;
		break;
	}

	switch (text->anchor) {
	case GTK_ANCHOR_NW:
	case GTK_ANCHOR_N:
	case GTK_ANCHOR_NE:
		break;

	case GTK_ANCHOR_W:
	case GTK_ANCHOR_CENTER:
	case GTK_ANCHOR_E:
		text->cy -= height / 2;
		text->clip_cy -= text->clip_cheight / 2;
		break;

	case GTK_ANCHOR_SW:
	case GTK_ANCHOR_S:
	case GTK_ANCHOR_SE:
		text->cy -= height;
		text->clip_cy -= text->clip_cheight;
		break;
	}

        /* Bounds */
	if (text->clip) {
                /* maybe do bbox intersection here? */
		*px1 = text->clip_cx;
		*py1 = text->clip_cy;
		*px2 = *px1 + text->clip_width;
		*py2 = *py1 + text->clip_height;
	} else {
                /* *px1 = text->cx + private->min_lbearing; */
		*px1 = text->cx;
		*py1 = text->cy;
                /* *px2 = text->cx + private->max_rbearing; */
		*px2 = *px1 + width;
		*py2 = *py1 + height;
	}

} /* get_bounds_relative */
#endif

static void
gnome_canvas_tktext_update (GnomeCanvasItem *item, double *affine,
			    ArtSVP *clip_path, int flags)
{
	double x1, y1, x2, y2;

	if (parent_class->update)
		(* parent_class->update) (item, affine, clip_path, flags);

#if 0
	if (item->canvas->aa) {
		/* anti-alias canvas mode */
		ArtDRect i_bbox, c_bbox;
		int i;

		for (i = 0; i < 6; i++)
			text->affine[i] = affine[i];

		get_bounds_relative(text, &i_bbox.x0, &i_bbox.y0,
				    &i_bbox.x1, &i_bbox.y1);
		art_drect_affine_transform(&c_bbox, &i_bbox, affine);
		gnome_canvas_update_bbox(item, c_bbox.x0, c_bbox.y0,
					 c_bbox.x1, c_bbox.y1);
	} else
#endif
	{
		get_bounds(GNOME_CANVAS_TKTEXT(item), &x1, &y1, &x2, &y2);
#if 0
		printf("bounding box: x1=%d, y1=%d, x2=%d, y2=%d\n",
		       (int)x1, (int)y1, (int)x2, (int)y2);
#endif
		gnome_canvas_update_bbox(item, x1, y1, x2, y2);
	}
} /* gnome_canvas_tktext_update */

/* Point handler for the text item */
static double
gnome_canvas_tktext_point (GnomeCanvasItem *item, double x, double y,
			   int cx, int cy, GnomeCanvasItem **actual_item)
{
	GnomeCanvasTkText *tkxt;
	gdouble dist;
	int nx, ny;

	g_return_val_if_fail(item, 0.0);

	tkxt = GNOME_CANVAS_TKTEXT (item);

	*actual_item = item;

	nx = (int)(((double)cx - item->x1) / item->canvas->pixels_per_unit);
	ny = (int)(((double)cy - item->y1) / item->canvas->pixels_per_unit);

	dist = gtk_text_layout_get_dist_from_pixel(tkxt->layout,
						   NULL,
						   nx,
						   ny);

	printf("point: x: %f, y: %f, cx: %d, cy: %d\nnx: %d, ny: %d, ppu: %f, dist: %f\n",
	       x, y, cx, cy, nx, ny, item->canvas->pixels_per_unit, dist);
	printf("x1: %f, y1: %f, x2: %f, y2: %f\n", item->x1, item->y1,
	       item->x2, item->y2);

	return dist / item->canvas->pixels_per_unit;
} /* gnome_canvas_tktext_point */

static void gnome_canvas_tktext_bounds (GnomeCanvasItem *item, double *x1,
					double *y1, double *x2, double *y2)
{
	printf("bounds called.\n");
}


/*
 * Selections
 */

gboolean
gnome_canvas_tktext_scroll_to_mark_adjusted (GnomeCanvasTkText *tkxt,
					     const gchar *mark_name,
					     gint margin,
					     gfloat percentage)
{
#if 0
	GtkTextIter iter;
	GtkTextRectangle rect;
	GtkTextRectangle screen;
	gint screen_bottom;
	gint screen_right;
	gboolean retval = FALSE;
	gint scroll_inc;
	gdouble x1, y1, x2, y2;

	g_return_val_if_fail(GNOME_IS_CANVAS_TKTEXT(tkxt), FALSE);
	g_return_val_if_fail(mark_name != NULL, FALSE);

	if (!gtk_text_buffer_get_iter_at_mark(tkxt->buffer, &iter,mark_name)) {
		g_warning("Mark %s does not exist! can't scroll to it.",
			  mark_name);
		return FALSE;
	}

	gtk_text_layout_get_iter_location(tkxt->layout, &iter, &rect);

	gnome_canvas_get_scroll_region(GNOME_CANVAS_ITEM(tkxt)->canvas,
				       &x1, &y1, &x2, &y2);

	screen.x = x1;
	screen.y = y1;
	screen.width = x2 - x1;
	screen.height = y2 - y1;

#if 1
	{
		/* Clamp margin so it's not too large. */
		gint small_dimension = MIN(screen.width, screen.height);
		gint max_rect_dim;

		if (margin > (small_dimension/2 - 5)) /* 5 is arbitrary */
			margin = (small_dimension/2 - 5);

		if (margin < 0)
			margin = 0;

		/* make sure rectangle fits in the leftover space */

		max_rect_dim = MAX(rect.width, rect.height);

		if (max_rect_dim > (small_dimension - margin*2))
			margin -= max_rect_dim - (small_dimension - margin*2);

		if (margin < 0)
			margin = 0;
	}

	g_assert(margin >= 0);

	screen.x += margin;
	screen.y += margin;

	screen.width -= margin*2;
	screen.height -= margin*2;
#endif

	screen_bottom = screen.y + screen.height;
	screen_right = screen.x + screen.width;

	/* Vertical scroll (only vertical gets adjusted) */

	scroll_inc = 0;
	if (rect.y < screen.y) {
		gint scroll_dest = rect.y;
		scroll_inc = (scroll_dest - screen.y) * percentage;
	} else if ((rect.y + rect.height) > screen_bottom) {
		gint scroll_dest = rect.y + rect.height;
		scroll_inc = (scroll_dest - screen_bottom) * percentage;
	}

	if (scroll_inc != 0) {
#if 0
		set_adjustment_clamped(GTK_LAYOUT(tkxt)->vadjustment,
				       ((gint)GTK_LAYOUT(tkxt)->yoffset) + scroll_inc);
#else
		gnome_canvas_scroll_to(GNOME_CANVAS_ITEM(tkxt)->canvas,
				       0, scroll_inc);
#endif
		retval = TRUE;
	}

	/* Horizontal scroll */

	scroll_inc = 0;
	if (rect.x < screen.x) {
		gint scroll_dest = rect.x;
		scroll_inc = scroll_dest - screen.x;
	} else if ((rect.x + rect.width) > screen_right) {
		gint scroll_dest = rect.x + rect.width;
		scroll_inc = scroll_dest - screen_right;
	}

	if (scroll_inc != 0) {
#if 0
		set_adjustment_clamped(GTK_LAYOUT(tkxt)->hadjustment,
				       ((gint)GTK_LAYOUT(tkxt)->xoffset) + scroll_inc);
#else
		gnome_canvas_scroll_to(GNOME_CANVAS_ITEM(tkxt)->canvas,
				       scroll_inc, 0);
#endif
		retval = TRUE;
	}

	return retval;
#else
	return FALSE;
#endif
} /*gnome_canvas_tktext_scroll_to_mark_adjusted */

static gboolean
move_insert_to_pointer_and_scroll(GnomeCanvasTkText *tkxt,
				  gboolean partial_scroll)
{
	gint x, y;
	gdouble wx, wy;
	GdkModifierType state;
	GtkTextIter newplace;
	gint adjust = 0;
	gboolean in_threshold = FALSE;
	gboolean scrolled = FALSE;
	GnomeCanvasItem *item;

	g_return_val_if_fail(tkxt, FALSE);

	item = GNOME_CANVAS_ITEM(tkxt);

	/* Grab the pointer location, frob it into the canvas coord system */
	gdk_window_get_pointer(GTK_LAYOUT(GNOME_CANVAS_ITEM(tkxt)->canvas)->bin_window, &x, &y, &state);
	gnome_canvas_window_to_world(item->canvas, (double)x, (double)y,
				     &wx, &wy);
	gnome_canvas_w2c(item->canvas, wx, wy, &x, &y);

	/* Adjust movement by how long we've been selecting, to
	   get an acceleration effect. The exact numbers are
	   pretty arbitrary. We have a threshold before we
	   start to accelerate. */
	/* uncommenting this printf helps visualize how it works. */     
	/* printf("%d\n", tkxt->scrolling_accel_factor); */

	if (tkxt->scrolling_accel_factor > 10)
		adjust = (tkxt->scrolling_accel_factor - 10) * 75;

	if (y < 0) /* scrolling upward */
		adjust = -adjust; 

	/* No adjust if the pointer has moved back inside the window for sure.
	   Also I'm adding a small threshold where no adjust is added,
	   in case you want to do a continuous slow scroll. */
#define SLOW_SCROLL_TH 7
#if 0
	{
		gint cx, cy;

		gnome_canvas_w2c(item->canvas, item->canvas->scroll_x2,
				 item->canvas->scroll_y2, &cx, &cy);
		printf("x: %d, y: %d, cx: %d, cy: %d\n",
		       x, y,  cx, cy);
		printf("sx1: %f, sy1: %f, sx2: %f, sy2: %f\n",
		       item->canvas->scroll_x1, item->canvas->scroll_y1,
		       item->canvas->scroll_x2, item->canvas->scroll_y2);
		if (x >= (0 - SLOW_SCROLL_TH) &&
		    x < (cx + SLOW_SCROLL_TH) &&
		    y >= (0 - SLOW_SCROLL_TH) &&
		    y < (cy + SLOW_SCROLL_TH)) {
			adjust = 0;
			in_threshold = TRUE;
		}
	}
#endif

	/* Also, for the canvas item, never try to scroll past the end of the
	 * bounding box.  The canvas item expands with the addition of text,
	 * not in response to the mouse. */
	if (x >= (item->x1 - SLOW_SCROLL_TH) &&
	    x < (item->x2 + SLOW_SCROLL_TH) &&
	    y >= (item->x1 - SLOW_SCROLL_TH) &&
	    y < (item->y2 + SLOW_SCROLL_TH)) {
		adjust = 0;
		in_threshold = TRUE;
	}

	gnome_canvas_tktext_get_iter_at_pixel(tkxt, &newplace,
					      x + tkxt->xofs,
					      y + tkxt->yofs + adjust);

	gtk_text_buffer_move_mark(tkxt->buffer, "insert", &newplace);

	if (partial_scroll)
		scrolled = gnome_canvas_tktext_scroll_to_mark_adjusted(tkxt, "insert", 0, 0.7);
	else
		scrolled = gnome_canvas_tktext_scroll_to_mark_adjusted(tkxt, "insert", 0, 1.0);

	if (scrolled) {
		/* We want to avoid rapid jump to super-accelerated when you
		   leave the slow scroll threshold after scrolling for a
		   while. So we slowly decrease accel when scrolling inside
		   the threshold. */
		if (in_threshold) {
			if (tkxt->scrolling_accel_factor > 1)
				tkxt->scrolling_accel_factor -= 2;
		} else
			tkxt->scrolling_accel_factor += 1;
	} else {
		/* If we don't scroll we're probably inside the window, but
		   potentially just a bit outside. We decrease acceleration
		   while the user is fooling around inside the window.
		   Acceleration decreases faster than it increases. */
		if (tkxt->scrolling_accel_factor > 4)
			tkxt->scrolling_accel_factor -= 5;
	}

	return scrolled;
} /* move_insert_to_pointer_and_scroll */

static gint
selection_scan_timeout(gpointer data)
{
	GnomeCanvasTkText *tkxt;

	tkxt = GNOME_CANVAS_TKTEXT(data);

	if (move_insert_to_pointer_and_scroll(tkxt, TRUE))
		return TRUE; /* remain installed. */
	else {
		tkxt->selection_drag_scan_timeout = 0;
		return FALSE; /* remove ourselves */
	}
} /* selection_scan_timeout */

static gint
selection_motion_event_handler(GnomeCanvasTkText *tkxt, GdkEventMotion *event)
{
	if (move_insert_to_pointer_and_scroll(tkxt, TRUE)) {
		/* If we had to scroll offscreen, insert a timeout to do so
		   again. Note that in the timeout, even if the mouse doesn't
		   move, due to this scroll xoffset/yoffset will have changed
		   and we'll need to scroll again. */
		if (tkxt->selection_drag_scan_timeout != 0) /* reset on every motion event */
			gtk_timeout_remove(tkxt->selection_drag_scan_timeout);
      
		tkxt->selection_drag_scan_timeout =
			gtk_timeout_add(50, selection_scan_timeout, tkxt);
	}

	return TRUE;
} /* selection_motion_event_handler */

static void
gnome_canvas_tktext_start_selection_drag(GnomeCanvasTkText *tkxt,
					 const GtkTextIter *iter,
					 GdkEventButton *event)
{
	GtkTextIter newplace;

	g_return_if_fail(tkxt->selection_drag_handler == 0);

	gnome_canvas_item_grab_focus(GNOME_CANVAS_ITEM(tkxt));

	tkxt->scrolling_accel_factor = 0;

	newplace = *iter;

	gtk_text_buffer_place_cursor(tkxt->buffer, &newplace);
	gnome_canvas_tktext_emit_cursor_event(tkxt, &newplace, FALSE);

	tkxt->selection_drag_handler = selection_motion_event_handler;
} /* gnome_canvas_tktext_start_selection_drag */

/* returns whether we were really dragging */
static gboolean
gnome_canvas_tktext_end_selection_drag(GnomeCanvasTkText *tkxt,
				       GdkEventButton *event)
{
	if (tkxt->selection_drag_handler == 0)
		return FALSE;

	tkxt->selection_drag_handler = NULL;

	tkxt->scrolling_accel_factor = 0;

	if (tkxt->selection_drag_scan_timeout != 0) {
		gtk_timeout_remove(tkxt->selection_drag_scan_timeout);
		tkxt->selection_drag_scan_timeout = 0;
	}

	/* one last update to current position */
	move_insert_to_pointer_and_scroll(tkxt, FALSE);

#if 0
	/* ungrab focus? -- phil*/
	gtk_grab_remove(GTK_WIDGET(tkxt));
#endif

	return TRUE;
} /* gnome_canvas_tktext_end_selection_drag */

#ifdef TKXT_DND
static void
gnome_canvas_tktext_start_selection_dnd(GnomeCanvasTkText *tkxt,
					const GtkTextIter *iter,
					GdkEventButton *event)
{
	GdkDragContext *context;
	GtkTargetList *target_list;
	GtkTextMark *mark;

	/* FIXME we have to handle more formats for the selection,
	   and do the conversions to/from UTF8 */

	/* FIXME not sure how this is memory-managed. */
	target_list = gtk_target_list_new (target_table, n_targets);

	context = gtk_drag_begin(GTK_WIDGET(GNOME_CANVAS_ITEM(tkxt)->canvas),
				 target_list,
				 GDK_ACTION_COPY | GDK_ACTION_MOVE,
				 1, (GdkEvent*)event);

	gtk_drag_set_icon_default (context);

	/* We're inside the selection, so start without being able
	   to accept the drag. */
	gdk_drag_status (context, 0, event->time);
	gtk_text_mark_set_visible(tkxt->dnd_mark, FALSE);
} /* gnome_canvas_tktext_start_selection_dnd */

static void
gnome_canvas_tktext_drag_begin(GnomeCanvasTkText *item,
			       GdkDragContext *context)
{
	/* I am on a train. */

	/* Trains are fast. */

	/* I like trains. */
}

static void
gnome_canvas_tktext_drag_end(GnomeCanvasItem *item,
			     GdkDragContext *context)
{
	GnomeCanvasTkText *tkxt;

	tkxt = GNOME_CANVAS_TKTEXT(item);

	gtk_text_mark_set_visible(tkxt->dnd_mark, FALSE);
} /* gnome_canvas_tktext_drag_end */
#endif

static void
gnome_canvas_tktext_need_repaint(GtkTextLayout *layout, gint x, gint y,
				 gint width, gint height,
				 GnomeCanvasTkText *tkxt)
{
#if 0
	gint cx, cy;
	gdouble wx, wy;

#endif
	GnomeCanvas *canvas;

	g_return_if_fail(tkxt);

	x += GNOME_CANVAS_ITEM(tkxt)->x1 - tkxt->xofs;
	y += GNOME_CANVAS_ITEM(tkxt)->y1 - tkxt->yofs;

	canvas = GNOME_CANVAS_ITEM(tkxt)->canvas;
#if 0
	gnome_canvas_c2w(canvas, x, y, &wx, &wy);

	cx = (int)wx;
	cy = (int)wy;
#endif

	gnome_canvas_request_redraw(GNOME_CANVAS_ITEM(tkxt)->canvas,
				    x, y,
				    x + width * canvas->pixels_per_unit,
				    y + height * canvas->pixels_per_unit);

#if 0
	{
	GnomeCanvasItem *item = GNOME_CANVAS_ITEM(tkxt);
	printf("bounding box: x1=%d, y1=%d, x2=%d, y2=%d\n",
	       (int)item->x1, (int)item->y1, (int)item->x2, (int)item->y2);
	/* printf("cx: %d, cy: %d, wx: %f, wy: %f\n", cx, cy, wx, wy); */
	printf("layout: %p, x: %d, y: %d, width: %d, height: %d, tkxt: %p\n",
	       layout, x, y, width, height, tkxt);
	}
#endif
} /* gnome_canvas_tktext_need_repaint */

/* FIXME: move these 3 (or four!) functions to non-view-specific things */
static inline gboolean
whitespace(GtkTextUniChar ch, gpointer user_data)
{
	return (ch == ' ' || ch == '\t');
}

static inline gboolean
not_whitespace(GtkTextUniChar ch, gpointer user_data)
{
	return !whitespace(ch, user_data);
}

static gboolean
find_whitepace_region(const GtkTextIter *center,
                      GtkTextIter *start, GtkTextIter *end)
{
	*start = *center;
	*end = *center;

	if (gtk_text_iter_backward_find_char(start, not_whitespace, NULL))
		/* we want the first whitespace... */
		gtk_text_iter_forward_char(start);
	if (whitespace(gtk_text_iter_get_char(end), NULL))
		gtk_text_iter_forward_find_char(end, not_whitespace, NULL);

	return !gtk_text_iter_equal(start, end);
} /* find_whitepace_region */

static void
gnome_canvas_tktext_set_anchor(GnomeCanvasTkText *tkxt)
{
	GtkTextIter insert;

	g_return_if_fail(tkxt);

	gtk_text_buffer_get_iter_at_mark(tkxt->buffer, &insert, "insert");

	gtk_text_buffer_create_mark(tkxt->buffer, "anchor", &insert, TRUE);
} /* gnome_canvas_tktext_set_anchor */

static void
gnome_canvas_tktext_delete_text(GnomeCanvasTkText *tkxt,
				GtkTextViewDeleteType type,
				gint count)
{
	GtkTextIter insert;
	GtkTextIter start;
	GtkTextIter end;
	gboolean leave_one = FALSE;

	g_return_if_fail(tkxt);

	if (type == GTK_TEXT_DELETE_CHAR) {
		/* Char delete deletes the selection, if one exists */
		if (gtk_text_buffer_delete_selection(tkxt->buffer))
			return;
	}

	gtk_text_buffer_get_iter_at_mark(tkxt->buffer, &insert, "insert");

	start = insert;
	end = insert;

	switch (type) {
	case GTK_TEXT_DELETE_CHAR:
		gtk_text_iter_forward_chars(&end, count);
		break;

	case GTK_TEXT_DELETE_HALF_WORD:
		if (count > 0)
			gtk_text_iter_forward_word_ends(&end, count);
		else if (count < 0)
			gtk_text_iter_backward_word_starts(&start, 0 - count);
		break;

	case GTK_TEXT_DELETE_WHOLE_WORD:
		break;

	case GTK_TEXT_DELETE_HALF_LINE:
		break;

	case GTK_TEXT_DELETE_WHOLE_LINE:
		break;

	case GTK_TEXT_DELETE_HALF_PARAGRAPH:
		while (count > 0) {
			if (!gtk_text_iter_forward_to_newline(&end))
				break;

			--count;
		}

		/* FIXME figure out what a negative count means
		   and support that */
		break;
      
	case GTK_TEXT_DELETE_WHOLE_PARAGRAPH:
		if (count > 0) {
			gtk_text_iter_set_line_char(&start, 0);
			gtk_text_iter_forward_to_newline(&end);

			/* Do the lines beyond the first. */
			while (count > 1) {
				gtk_text_iter_forward_to_newline(&end);

				--count;
			}
		}

		/* FIXME negative count? */

		break;

	case GTK_TEXT_DELETE_WHITESPACE_LEAVE_ONE:
		leave_one = TRUE; /* FALL THRU */
	case GTK_TEXT_DELETE_WHITESPACE: {
		find_whitepace_region(&insert, &start, &end);
	}
	break;

	default:
		break;
	}

	if (!gtk_text_iter_equal(&start, &end)) {
		gtk_text_buffer_delete(tkxt->buffer, &start, &end);

		if (leave_one)
			gtk_text_buffer_insert_at_cursor(tkxt->buffer, " ", 1);
	}
} /* gnome_canvas_tktext_delete_text */

static void
gnome_canvas_tktext_move_insert(GnomeCanvasTkText *tkxt,
				GtkTextViewMovementStep step,
				gint count,
				gboolean extend_selection)
{
	GtkTextIter insert;
	GtkTextIter newplace;

	g_return_if_fail(tkxt);

	gtk_text_buffer_get_iter_at_mark(tkxt->buffer, &insert, "insert");
	newplace = insert;

	switch (step) {
	case GTK_TEXT_MOVEMENT_CHAR:
		gtk_text_iter_forward_chars(&newplace, count);
		break;

	case GTK_TEXT_MOVEMENT_WORD:
		if (count < 0)
			gtk_text_iter_backward_word_starts(&newplace, -count);
		else if (count > 0)
			gtk_text_iter_forward_word_ends(&newplace, count);
		break;

	case GTK_TEXT_MOVEMENT_LINE:
		break;

	case GTK_TEXT_MOVEMENT_PARAGRAPH:
		gtk_text_iter_down_lines(&newplace, count);
		break;

	case GTK_TEXT_MOVEMENT_PARAGRAPH_ENDS:
		if (count > 0)
			gtk_text_iter_forward_to_newline(&newplace);
		else if (count < 0)
			gtk_text_iter_set_line_char(&newplace, 0);
		break;

	case GTK_TEXT_MOVEMENT_BUFFER_ENDS:
		if (count > 0)
			gtk_text_buffer_get_last_iter(tkxt->buffer, &newplace);
		else if (count < 0)
			gtk_text_buffer_get_iter_at_char(tkxt->buffer,
							 &newplace, 0);
		break;

	default:
		break;
	}

	if (!gtk_text_iter_equal(&insert, &newplace)) {
		if (extend_selection)
			gtk_text_buffer_move_mark(tkxt->buffer, "insert", &newplace);
		else {
			gtk_text_buffer_place_cursor(tkxt->buffer, &newplace);
			gnome_canvas_tktext_emit_cursor_event(tkxt, &newplace,
							      FALSE);
		}
	}
} /* gnome_canvas_tktext_move_insert */

static void
gnome_canvas_tktext_cut_text (GnomeCanvasTkText *tkxt)
{
	gtk_text_buffer_cut(tkxt->buffer, GDK_CURRENT_TIME);
} /* gnome_canvas_tktext_cut_text */

static void
gnome_canvas_tktext_copy_text (GnomeCanvasTkText *tkxt)
{
	gtk_text_buffer_copy(tkxt->buffer, GDK_CURRENT_TIME);
} /* gnome_canvas_tktext_copy_text */

static void
gnome_canvas_tktext_paste_text (GnomeCanvasTkText *tkxt)
{
	gtk_text_buffer_paste_clipboard(tkxt->buffer, GDK_CURRENT_TIME);
} /* gnome_canvas_tktext_paste_text */

static void
gnome_canvas_tktext_toggle_overwrite(GnomeCanvasTkText *tkxt)
{
	g_return_if_fail(tkxt);

	tkxt->overwrite_mode = !tkxt->overwrite_mode;
} /* gnome_canvas_tktext_toggle_overwrite */

























#if 0
#if 0
static void
get_bounds_item_relative (GnomeCanvasTkText *text, double *px1,
			  double *py1, double *px2, double *py2)
{
	GnomeCanvasItem *item;
	double x, y;
	double clip_x, clip_y;

	item = GNOME_CANVAS_ITEM (text);

	clip_x = x = text->x;
	clip_y = y = text->y;

	/* Calculate text dimensions */
	calc_text_height(text);

	/* Anchor text */
	switch (text->anchor) {
	case GTK_ANCHOR_NW:
	case GTK_ANCHOR_W:
	case GTK_ANCHOR_SW:
		break;

	case GTK_ANCHOR_N:
	case GTK_ANCHOR_CENTER:
	case GTK_ANCHOR_S:
		x -= text->max_width / 2;
		clip_x -= text->clip_width / 2;
		break;

	case GTK_ANCHOR_NE:
	case GTK_ANCHOR_E:
	case GTK_ANCHOR_SE:
		x -= text->max_width;
		clip_x -= text->clip_width;
		break;
	}

	switch (text->anchor) {
	case GTK_ANCHOR_NW:
	case GTK_ANCHOR_N:
	case GTK_ANCHOR_NE:
		break;

	case GTK_ANCHOR_W:
	case GTK_ANCHOR_CENTER:
	case GTK_ANCHOR_E:
		y -= text->height / 2;
		clip_y -= text->clip_height / 2;
		break;

	case GTK_ANCHOR_SW:
	case GTK_ANCHOR_S:
	case GTK_ANCHOR_SE:
		y -= text->height;
		clip_y -= text->clip_height;
		break;
	}

	/* Bounds */
	if (text->clip) {
		/* maybe do bbox intersection here? */
		*px1 = clip_x;
		*py1 = clip_y;
		*px2 = clip_x + text->clip_width;
		*py2 = clip_y + text->clip_height;
	} else {
		*px1 = x;
		*py1 = y;
		*px2 = x + text->max_width;
		*py2 = y + text->height;
	}
} /* get_bounds_item_relative */
#endif

#if 1
/* I got rid of the clip rectangle, which may or may not be the right thing to
 * do. */
static void
get_bounds (GnomeCanvasTkText *text, int *px1, int *py1, int *px2, int *py2)
{
	GnomeCanvasItem *item;
	double wx, wy;

	item = GNOME_CANVAS_ITEM (text);

	/* Get canvas pixel coordinates for text position */
	wx = text->x;
	wy = text->y;
	gnome_canvas_item_i2w (item, &wx, &wy);
	gnome_canvas_w2c (item->canvas, wx + text->xofs, wy + text->yofs,
			  &text->cx, &text->cy);

	/* Calculate text dimensions */
	calc_text_height(text);

	/* Anchor text */
	switch (text->anchor) {
	case GTK_ANCHOR_NW:
	case GTK_ANCHOR_W:
	case GTK_ANCHOR_SW:
		break;

	case GTK_ANCHOR_N:
	case GTK_ANCHOR_CENTER:
	case GTK_ANCHOR_S:
		text->cx -= text->max_width / 2;
		break;

	case GTK_ANCHOR_NE:
	case GTK_ANCHOR_E:
	case GTK_ANCHOR_SE:
		text->cx -= text->max_width;
		break;
	}

	switch (text->anchor) {
	case GTK_ANCHOR_NW:
	case GTK_ANCHOR_N:
	case GTK_ANCHOR_NE:
		break;

	case GTK_ANCHOR_W:
	case GTK_ANCHOR_CENTER:
	case GTK_ANCHOR_E:
		text->cy -= text->height / 2;
		break;

	case GTK_ANCHOR_SW:
	case GTK_ANCHOR_S:
	case GTK_ANCHOR_SE:
		text->cy -= text->height;
		break;
	}

	/* Bounds */
	*px1 = text->cx;
	*py1 = text->cy;
	*px2 = text->cx + text->max_width + 3 /* cursor */ ;
	*py2 = text->cy + text->height;
} /* get_bounds */
#else
#endif

#if 0
/* Calculates the x position of the specified line of text, based on the text's
 * justification */
static double
get_line_xpos_item_relative (GnomeCanvasText *text, struct line *line)
{
	double x;

	x = text->x;

	switch (text->anchor) {
	case GTK_ANCHOR_NW:
	case GTK_ANCHOR_W:
	case GTK_ANCHOR_SW:
		break;

	case GTK_ANCHOR_N:
	case GTK_ANCHOR_CENTER:
	case GTK_ANCHOR_S:
		x -= text->max_width / 2;
		break;

	case GTK_ANCHOR_NE:
	case GTK_ANCHOR_E:
	case GTK_ANCHOR_SE:
		x -= text->max_width;
		break;
	}

	switch (text->justification) {
	case GTK_JUSTIFY_RIGHT:
		x += text->max_width - line->width;
		break;

	case GTK_JUSTIFY_CENTER:
		x += (text->max_width - line->width) * 0.5;
		break;

	default:
		/* For GTK_JUSTIFY_LEFT, we don't have to do anything.  We do
		 * not support GTK_JUSTIFY_FILL, yet.
		 */
		break;
	}

	return x;
} /* get_line_xpos_item_relative */

/* Calculates the y position of the first line of text. */
static double
get_line_ypos_item_relative (GnomeCanvasText *text)
{
	double y;

	y = text->y;

	switch (text->anchor) {
	case GTK_ANCHOR_NW:
	case GTK_ANCHOR_N:
	case GTK_ANCHOR_NE:
		break;

	case GTK_ANCHOR_W:
	case GTK_ANCHOR_CENTER:
	case GTK_ANCHOR_E:
		y -= text->height / 2;
		break;

	case GTK_ANCHOR_SW:
	case GTK_ANCHOR_S:
	case GTK_ANCHOR_SE:
		y -= text->height;
		break;
	}

	return y;
} /* get_line_ypos_item_relative */
#endif

/* Calculates the x position of the specified line of text, based on the text's * justification */
static int
get_line_xpos (GnomeCanvasTkText *text, GnomeCanvasTkTextLine *line)
{
	int x;

	x = text->cx;

	switch (text->justification) {
	case GTK_JUSTIFY_RIGHT:
		x += text->max_width - line->width;
		break;

	case GTK_JUSTIFY_CENTER:
		x += (text->max_width - line->width) / 2;
		break;

	default:
		/* For GTK_JUSTIFY_LEFT, we don't have to do anything.  We do
		 * not support GTK_JUSTIFY_FILL, yet.
		 */
		break;
	}

	return x;
} /* get_line_xpos */
#endif
