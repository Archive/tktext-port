#ifndef G_BSEARCH_H
#define G_BSEARCH_H

#include <glib.h>

gpointer g_bsearch (gconstpointer key, gconstpointer array,
                    guint n_elements, guint element_size,
                    GCompareFunc compare_func);
#endif

