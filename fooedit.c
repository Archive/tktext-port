/* Cheesy text editor for testing the text widget */

#include "frootkxt.h"
#include <gnome.h>
#include <errno.h>
#define _GNU_SOURCE
#include <stdio.h>

static void open_document(const gchar* filename);

static void create_view(FrooTkxtBuffer* buffer);

static void session_die(GnomeClient* client, gpointer client_data);

static gint save_session(GnomeClient *client, gint phase, 
                         GnomeSaveStyle save_style,
                         gint is_shutdown, GnomeInteractStyle interact_style,
                         gint is_fast, gpointer client_data);
static gint delete_event_cb(GtkWidget* w, GdkEventAny* e, gpointer data);

static void install_menus_and_toolbar(GtkWidget* app);

static void edit_tags(FrooTkxtBuffer* buffer);

struct poptOption options[] = {
  {
    NULL,
    '\0',
    0,
    NULL,
    0,
    NULL,
    NULL
  }
};

int
main(int argc, char** argv)
{
  poptContext pctx;

  char** args;
  int i;

  GSList* files = NULL;
  GSList* iter;
  
  GnomeClient* client;

#if 0
  bindtextdomain(PACKAGE, GNOMELOCALEDIR);  
  textdomain(PACKAGE);
#endif

#define PACKAGE "fooedit"
#define VERSION "157.1.2.79.23.62"
  gnome_init_with_popt_table(PACKAGE, VERSION, argc, argv, 
                             options, 0, &pctx);  

  /* Argument parsing */

  args = poptGetArgs(pctx);

  if (args)
    {
      i = 0;
      while (args[i] != NULL) 
        {
          files = g_slist_prepend(files, args[i]);
          ++i;
        }
      /* Put them in order */
      files = g_slist_reverse(files); 
    }

  /* Session Management */
  
  client = gnome_master_client ();
  gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
                      GTK_SIGNAL_FUNC (save_session), argv[0]);
  gtk_signal_connect (GTK_OBJECT (client), "die",
                      GTK_SIGNAL_FUNC (session_die), NULL);

  
  /* Main app */
  iter = files;
  while (iter != NULL)
    {
      const gchar* file = iter->data;

      open_document(file);
      
      iter = g_slist_next(iter);
    }

  g_slist_free(files);

  poptFreeContext(pctx);

  gtk_main();

  return 0;
}

static gint
save_session (GnomeClient *client, gint phase, GnomeSaveStyle save_style,
              gint is_shutdown, GnomeInteractStyle interact_style,
              gint is_fast, gpointer client_data)
{
  gchar** argv;
  guint argc;

  /* allocate 0-filled, so it will be NULL-terminated */
  argv = g_malloc0(sizeof(gchar*)*4);
  argc = 1;

  argv[0] = client_data;

  /* FIXME fill in remainining argv with files to re-open, and perhaps
     the working directory */
  
  gnome_client_set_clone_command (client, argc, argv);
  gnome_client_set_restart_command (client, argc, argv);

  return TRUE;
}

static void
session_die(GnomeClient* client, gpointer client_data)
{
  gtk_main_quit ();
}

static void
open_document(const gchar* filename)
{
  FrooTkxtBuffer* buffer;
  FILE* f;
  gchar* line;
  size_t len;
  
  f = fopen(filename, "r");

  if (f == NULL)
    {
      gchar* msg;

      msg = g_strdup_printf(_("Error opening file %s: %s"),
                            filename, strerror(errno));

      gnome_error_dialog(msg);

      g_free(msg);
      return;
    }
  
  buffer = froo_tkxt_buffer_new(NULL);
  
  line = NULL;
  len = 0;
  while (getline(&line, &len, f) >= 0)
    {
      FrooTkxtIndex* index;

      len = strlen(line);
      
      index = froo_tkxt_buffer_get_last_index(buffer);
      froo_tkxt_buffer_insert_at_index(buffer, index, line, len);
      froo_tkxt_index_unref(index);
      
      free(line);
      
      line = NULL;
      len = 0;
    }

  if (!feof(f))
    {
      gchar* msg;

      msg = g_strdup_printf(_("Error reading file %s: %s"),
                            filename, strerror(errno));

      gnome_error_dialog(msg);

      g_free(msg);
    }
  
  fclose(f);

  {
    FrooTkxtTag* tag;
    
    tag = froo_tkxt_buffer_create_tag(buffer, "bg_blue");
    
    gtk_object_set(GTK_OBJECT(tag),
                   "background", "blue",
                   "font", "-*-courier-bold-r-*-*-20-*-*-*-*-*-*-*",
                   NULL);

    froo_tkxt_buffer_apply_tag(buffer, "bg_blue", 0, 30);
  }
  
  create_view(buffer);
}

static void
create_view(FrooTkxtBuffer* buffer)
{
  GtkWidget* app;
  GtkWidget* view;
  GtkWidget* status;
  GtkWidget* sw;
  
  app = gnome_app_new(PACKAGE, _("Text Editor (testing text widget)"));
  
  gtk_window_set_policy(GTK_WINDOW(app), FALSE, TRUE, FALSE);
  gtk_window_set_default_size(GTK_WINDOW(app), 400, 550);
  gtk_window_set_wmclass(GTK_WINDOW(app), "footext", "FooText");

  view = froo_tkxt_new();

  froo_tkxt_set_buffer(FROO_TKXT(view), buffer);
  
  sw = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw),
                                 GTK_POLICY_AUTOMATIC,
                                 GTK_POLICY_AUTOMATIC);

  gtk_container_add(GTK_CONTAINER(sw), view);
  
  gnome_app_set_contents(GNOME_APP(app), sw);

  status = gnome_appbar_new(FALSE, TRUE, GNOME_PREFERENCES_NEVER);

  gnome_app_set_statusbar(GNOME_APP(app), status);

  install_menus_and_toolbar(app);
  
  gtk_signal_connect(GTK_OBJECT(app),
                     "delete_event",
                     GTK_SIGNAL_FUNC(delete_event_cb),
                     NULL);

  gtk_object_set_data(GTK_OBJECT(app), "buffer", buffer);
  
  gtk_widget_show_all(app);
}

static gint 
delete_event_cb(GtkWidget* window, GdkEventAny* e, gpointer data)
{
  gtk_widget_destroy(window);

  /* Prevent the window's destruction, since we destroyed it 
   * ourselves
   */
  return TRUE;
}

/*
 * Menus
 */


static void nothing_cb(GtkWidget* widget, gpointer data);
static void edit_tags_cb(GtkWidget* widget, gpointer data);
static void new_app_cb(GtkWidget* widget, gpointer data);
static void close_cb  (GtkWidget* widget, gpointer data);
static void exit_cb   (GtkWidget* widget, gpointer data);
static void about_cb  (GtkWidget* widget, gpointer data);


static GnomeUIInfo file_menu [] = {
  GNOMEUIINFO_MENU_NEW_ITEM(N_("_New"),
                            N_("Create a new view"),
                            new_app_cb, NULL),

  GNOMEUIINFO_MENU_OPEN_ITEM(nothing_cb, NULL),

  GNOMEUIINFO_MENU_SAVE_ITEM(nothing_cb, NULL),

  GNOMEUIINFO_MENU_SAVE_AS_ITEM(nothing_cb, NULL),

  GNOMEUIINFO_SEPARATOR,

  GNOMEUIINFO_MENU_CLOSE_ITEM(close_cb, NULL),

  GNOMEUIINFO_MENU_EXIT_ITEM(exit_cb, NULL),

  GNOMEUIINFO_END
};

static GnomeUIInfo edit_menu [] = {
  GNOMEUIINFO_MENU_CUT_ITEM(nothing_cb, NULL), 
  GNOMEUIINFO_MENU_COPY_ITEM(nothing_cb, NULL),
  GNOMEUIINFO_MENU_PASTE_ITEM(nothing_cb, NULL),
  GNOMEUIINFO_MENU_CLEAR_ITEM(nothing_cb, NULL),
  GNOMEUIINFO_MENU_SELECT_ALL_ITEM(nothing_cb, NULL), 
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_MENU_FIND_ITEM(nothing_cb, NULL), 
  GNOMEUIINFO_MENU_FIND_AGAIN_ITEM(nothing_cb, NULL), 
  GNOMEUIINFO_MENU_REPLACE_ITEM(nothing_cb, NULL),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_MENU_PROPERTIES_ITEM(edit_tags_cb, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo help_menu [] = {
  GNOMEUIINFO_MENU_ABOUT_ITEM(about_cb, NULL),
  
  GNOMEUIINFO_END
};

static GnomeUIInfo menu [] = {
  GNOMEUIINFO_MENU_FILE_TREE(file_menu),
  GNOMEUIINFO_MENU_EDIT_TREE(edit_menu),
  GNOMEUIINFO_MENU_HELP_TREE(help_menu),
  GNOMEUIINFO_END
};

static GnomeUIInfo toolbar [] = {
  GNOMEUIINFO_ITEM_STOCK (N_("New"), N_("Create a new text file"), nothing_cb, GNOME_STOCK_PIXMAP_NEW),

  GNOMEUIINFO_SEPARATOR,

  GNOMEUIINFO_ITEM_STOCK (N_("Cut"), N_("Cut to clipboard"), nothing_cb, GNOME_STOCK_PIXMAP_CUT),
  GNOMEUIINFO_ITEM_STOCK (N_("Copy"), N_("Copy to clipboard"), nothing_cb, GNOME_STOCK_PIXMAP_COPY),
  GNOMEUIINFO_ITEM_STOCK (N_("Paste"), N_("Paste from clipboard"), nothing_cb, GNOME_STOCK_PIXMAP_PASTE),
  
  GNOMEUIINFO_END
};


static void 
install_menus_and_toolbar(GtkWidget* app)
{
  gnome_app_create_toolbar_with_data(GNOME_APP(app), toolbar, app);
  gnome_app_create_menus_with_data(GNOME_APP(app), menu, app);
  gnome_app_install_menu_hints(GNOME_APP(app), menu);
}

static void 
nothing_cb(GtkWidget* widget, gpointer data)
{
  GtkWidget* dialog;
  GtkWidget* app;
  
  app = (GtkWidget*) data;

  dialog = gnome_ok_dialog_parented(_("This does nothing."),
                                    GTK_WINDOW(app));
}

static void
edit_tags_cb(GtkWidget* widget, gpointer data)
{
  GtkWidget* app;
  FrooTkxtBuffer* buffer;
  
  app = (GtkWidget*) data;
  
  buffer = gtk_object_get_data(GTK_OBJECT(app), "buffer");

  edit_tags(buffer);
}

static void 
new_app_cb(GtkWidget* widget, gpointer data)
{
  FrooTkxtBuffer* buffer;

  buffer = froo_tkxt_buffer_new(NULL);
  
  create_view(buffer);
}

static void 
close_cb(GtkWidget* widget, gpointer data)
{
  GtkWidget* app;

  app = (GtkWidget*) data;

  gtk_widget_destroy(app);
}

static void 
exit_cb(GtkWidget* widget, gpointer data)
{
  gtk_main_quit();
}

static void 
about_cb(GtkWidget* widget, gpointer data)
{
  static GtkWidget* dialog = NULL;
  GtkWidget* app;

  app = (GtkWidget*) data;

  if (dialog != NULL) 
    {
      g_assert(GTK_WIDGET_REALIZED(dialog));
      gdk_window_show(dialog->window);
      gdk_window_raise(dialog->window);
    }
  else
    {        
      const gchar *authors[] = {
        "Havoc Pennington <hp@redhat.com>",
        NULL
      };

      dialog = gnome_about_new (_("Text widget test program"), VERSION,
                                "(C) 2000 Red Hat, Inc.",
                                authors,
                                _("Cheesy editor to test the text widget"),
                                NULL);

      gtk_signal_connect(GTK_OBJECT(dialog),
                         "destroy",
                         GTK_SIGNAL_FUNC(gtk_widget_destroyed),
                         &dialog);

      gnome_dialog_set_parent(GNOME_DIALOG(dialog), GTK_WINDOW(app));

      gtk_widget_show(dialog);
    }
}

static void
pixels_above_lines_cb(GtkWidget* entry, FrooTkxtTag* tag);     

static void
edit_tag(GtkWidget* button, GtkWidget* win)
{
  GtkWidget* editor;
  FrooTkxtBuffer* buffer;
  FrooTkxtTag* tag;
  GtkWidget* tagwin;
  GtkWidget* vbox;
  GtkWidget* hbox;
  GtkWidget* label;
  GtkWidget* entry;
  
  buffer = gtk_object_get_data(GTK_OBJECT(win), "buffer");

  tag = froo_tkxt_tag_table_lookup(buffer->tag_table, "bg_blue");

  tagwin = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  vbox = gtk_vbox_new(FALSE, 10);

  hbox = gtk_hbox_new(FALSE, 10);

  entry = gtk_entry_new();

  label = gtk_label_new("pixels_above_lines");
  
  gtk_container_add(GTK_CONTAINER(tagwin), vbox);

  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 10);

  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 10);
  gtk_box_pack_end(GTK_BOX(hbox), entry, FALSE, FALSE, 10);

  gtk_signal_connect(GTK_OBJECT(entry), "activate",
                     GTK_SIGNAL_FUNC(pixels_above_lines_cb),
                     tag);
  
  gtk_widget_show_all(tagwin);
}

static void
edit_tags(FrooTkxtBuffer* buffer)
{
  GtkWidget* win;
  GtkWidget* hbox;
  GtkWidget* vbox;
  GtkWidget* clist;
  GtkWidget* button;
  gchar* titles[] = { "Tag" };
  
  win = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  clist = gtk_clist_new_with_titles(1, titles);

  hbox = gtk_hbox_new(FALSE, 10);

  vbox = gtk_vbox_new(TRUE, 10);

  button = gtk_button_new_with_label("Edit");

  gtk_container_add(GTK_CONTAINER(win), hbox);
  gtk_box_pack_start(GTK_BOX(hbox), clist, TRUE, TRUE, 10);
  gtk_box_pack_end(GTK_BOX(hbox), vbox, FALSE, FALSE, 10);

  gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 10);

  gtk_signal_connect(GTK_OBJECT(button), "clicked", GTK_SIGNAL_FUNC(edit_tag), win);

  gtk_object_set_data(GTK_OBJECT(win), "buffer", buffer);
  
  gtk_widget_show_all(win);
}

static void
pixels_above_lines_cb(GtkWidget* entry, FrooTkxtTag* tag)
{
  gchar* str;

  str = gtk_editable_get_chars(GTK_EDITABLE(entry), 0, -1);

  if (str)
    {
      int pixels = atoi(str);
      gtk_object_set(GTK_OBJECT(tag),
                     "pixels_above_lines", pixels, NULL);
      g_free(str);
    }
}
