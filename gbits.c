
#include "gbits.h"

/* This may require some sort of configure check */
typedef gulong GWord;

struct _GBitVector {
  guint n_bits;
  guint words_allocated;
  GWord* bits;  
};

#define BITS_IN_WORDS(w) (sizeof(GWord)*8*(w))
#define WORDS_TO_FIT_BITS(b) ((b)/(8*sizeof(GWord)) + 1)
#define WORDS_IN_BITS(b) ((b)/(8*sizeof(GWord)))
#define WORD_END (BITS_IN_WORDS(1))

static void g_bit_vector_maybe_expand(GBitVector* vec, guint len);

static GWord word_masks[8*sizeof(GWord)];

#define GET_BIT(w, i) ( ((w) & (word_masks[(i)])) != 0 )
#define SET_BIT(w, i) ((w) |= (word_masks[(i)]))
#define UNSET_BIT(w, i) ((w) &= ~(word_masks[(i)]))

static gboolean initted = FALSE;

static void
g_bit_vector_init(void)
{
  guint i = 0;

  while (i < sizeof(word_masks)/sizeof(word_masks[0]))
    {
      /* word_masks[i] is a GWord with bit "i" turned on. */
      
      word_masks[i] = 1 << i;

      ++i;
    }
}

GBitVector*
g_bit_vector_new(void)
{
  GBitVector* vec;

  if (!initted)
    g_bit_vector_init();
  
  vec = g_new(GBitVector, 1);

  return vec;
}

void
g_bit_vector_free(GBitVector* vec)
{
  g_return_if_fail(vec != NULL);
  
  g_free(vec->bits);
  g_free(vec);
}

gboolean
g_bit_vector_get(GBitVector* vec, guint index)
{
  GWord* word;
  guint offset;
  guint wordbit;
  
  g_return_val_if_fail(vec != NULL, FALSE);
  g_return_val_if_fail(index < vec->n_bits, FALSE);

  offset = WORDS_IN_BITS(index);
  
  word = vec->bits + offset;

  wordbit = index - BITS_IN_WORDS(offset);

  g_assert(wordbit < sizeof(GWord)*8);
  
  return GET_BIT(*word, wordbit); 
}

void
g_bit_vector_set(GBitVector* vec, guint index, gboolean value)
{
  GWord* word;
  guint offset;
  guint wordbit;
  
  g_return_if_fail(vec != NULL);
  g_return_if_fail(index < vec->n_bits);
  
  offset = WORDS_IN_BITS(index);
  
  word = vec->bits + offset;

  wordbit = index - BITS_IN_WORDS(offset);

  g_assert(wordbit < sizeof(GWord)*8);
  
  if (value)
    SET_BIT(*word, wordbit);
  else
    UNSET_BIT(*word, wordbit);
}

void
g_bit_vector_set_all(GBitVector* vec, gboolean value)
{
  g_return_if_fail(vec != NULL);

  if (value)
    memset(vec->bits, ~((guchar)0), vec->words_allocated*sizeof(GWord));
  else
    memset(vec->bits, 0, vec->words_allocated*sizeof(GWord));
}

void
g_bit_vector_append(GBitVector* vec, gboolean value)

{
  g_return_if_fail(vec != NULL);
  
  g_bit_vector_maybe_expand(vec, vec->n_bits + 1);

  g_bit_vector_set(vec, vec->n_bits, value);
  
  vec->n_bits += 1;
}

void
g_bit_vector_set_size(GBitVector* vec, guint len)
{
  g_return_if_fail(vec != NULL);
  
  vec->n_bits = len;
  g_bit_vector_maybe_expand(vec, vec->n_bits);
}

guint
g_bit_vector_size(GBitVector* vec)
{
  g_return_val_if_fail(vec != NULL, 0);

  return vec->n_bits;
}

static guint
find_set_in_word(GWord word, guint start, guint end)
{
  guint i = start;

  g_assert(end <= WORD_END);
  
  while (i < end)
    {
      if (GET_BIT(word, i))
        return i;
      ++i;
    }
  return end;
}

static guint
find_unset_in_word(GWord word, guint start, guint end)
{
  guint i = start;

  g_assert(end <= WORD_END);
  
  while (i < end)
    {
      if (!GET_BIT(word, i))
        return i;
      ++i;
    }
  return end;
}

static guint
find_in_word(GWord word, guint start, guint end, gboolean val)
{
  if (val)
    return find_set_in_word(word, start, end);
  else
    return find_unset_in_word(word, start, end);
}

guint
g_bit_vector_find (GBitVector* vec, guint start, gboolean val)
{
  GWord* word;
  GWord* endword;
  guint wordbit;
  guint in_first, in_last;
  guint endbit;
  guint offset;
  
  g_return_val_if_fail(vec != NULL, 0);
  g_return_val_if_fail(start < vec->n_bits, FALSE);

  offset = WORDS_IN_BITS(start);
  
  word = vec->bits + offset;

  wordbit = start - BITS_IN_WORDS(offset);

  g_assert(wordbit < WORD_END);

  /* Check for unset/set bits in the remainder of the
     first word */
  endbit = vec->n_bits - BITS_IN_WORDS(offset);
  if (endbit > WORD_END)
    endbit = WORD_END;
  
  in_first = find_in_word(*word, wordbit, endbit, val);
  
  if (in_first != endbit)
    return BITS_IN_WORDS(offset) + in_first;
  
  if (endbit < WORD_END)
    return vec->n_bits; /* bits exhausted */
  
  /* Now for each whole word left in the range,
     see if it has any bits set/unset */

  ++word; /* go past initial word */
  offset = WORDS_IN_BITS(vec->n_bits);
  g_assert(offset > 0);
  endword = vec->bits + offset;

  /* This is the time-critical inner loop I guess */
  if (val)
    {
      while (word != endword)
        {
          if (*word != 0x0)
            break;
          ++word;
        }
    }
  else
    {
      while (word != endword)
        {
          if (*word != ~((GWord)0x0))
            break;
          ++word;
        }
    }

  if (word != endword)
    {
      guint found;
      guint bits_so_far;
          
      found = find_in_word(*word, 0, WORD_END, val);

      g_assert(found < WORD_END);

      bits_so_far = BITS_IN_WORDS(word - vec->bits);

      g_assert(bits_so_far < vec->n_bits);
          
      found += bits_so_far;

      g_assert(found < vec->n_bits);

      return found;
    }
  else
    {
      /* For the last partial word, see if we have any bits
         set/unset */

      endbit = vec->n_bits - BITS_IN_WORDS(offset);

      g_assert(endbit < WORD_END);

      in_last = find_in_word(*word, 0, endbit, val);

      if (in_last != endbit)
        return BITS_IN_WORDS(offset) + in_last;
  
      return vec->n_bits; /* bits exhausted */
    }
}

static gint
g_nearest_pow (gint num)
{
  gint n = 1;

  while (n < num)
    n <<= 1;

  return n;
}

#define MIN_VEC_SIZE 2

static void
g_bit_vector_maybe_expand(GBitVector* vec, guint len)
{
  guint want_alloc = WORDS_TO_FIT_BITS(len);

  if (want_alloc > vec->words_allocated)
    {
      vec->words_allocated = g_nearest_pow (want_alloc);
      vec->words_allocated = MAX (vec->words_allocated, MIN_VEC_SIZE);

      vec->bits = g_realloc (vec->bits, vec->words_allocated*sizeof(GWord));
    }
}

#include <stdio.h>

static void
test_find(GBitVector* vec, guint size, gboolean val)
{
  guint i;
  
  /* set/unset each bit, then try to find that bit */
  i = 0;
  while (i < g_bit_vector_size(vec))
    {
      guint found;
      
      g_bit_vector_set_all(vec, !val);

      /* Verify that all bits are set/unset */
      found = g_bit_vector_find(vec, 0, val);

      if (found != g_bit_vector_size(vec))
        g_error("Found %u should have found nothing", found);
      
      g_bit_vector_set(vec, i, val);

      /* check starting at i */
      found = g_bit_vector_find(vec, i, val);

      if (found != i)
        g_error("Found %u should have found %u", found, i);

      if (i < (size-1))
        {
          /* check starting after i that we find nothing */      
          found = g_bit_vector_find(vec, i+1, val);
          
          if (found != g_bit_vector_size(vec))
            g_error("Found %u should have found nothing", found);
        }
          
      /* check starting at 0 */
      found = g_bit_vector_find(vec, 0, val);
      
      if (found != i)
        g_error("Found %u should have found %u", found, i);
      
      ++i;
    }
}

static void
test_with_size(GBitVector* vec, guint size)
{
  guint i;

  printf("testing with size %u\n", size);
  
  /* set/get size */
  
  g_bit_vector_set_size(vec, size);

  g_assert(g_bit_vector_size(vec) == size);


  /* set_all */
  
  g_bit_vector_set_all(vec, TRUE);

  i = 0;
  while (i < g_bit_vector_size(vec))
    {
      g_assert(g_bit_vector_get(vec, i) == TRUE);
      
      ++i;
    }

  g_bit_vector_set_all(vec, FALSE);

  i = 0;
  while (i < g_bit_vector_size(vec))
    {
      g_assert(g_bit_vector_get(vec, i) == FALSE);
      
      ++i;
    }

  /* Set/get individual bits */

  /* every other bit */
  i = 0;
  while (i < g_bit_vector_size(vec))
    {
      g_bit_vector_set(vec, i, (i % 2) == 0 ? TRUE : FALSE);
      
      ++i;
    }

  i = 0;
  while (i < g_bit_vector_size(vec))
    {
      g_assert(g_bit_vector_get(vec, i) == ((i % 2) == 0 ? TRUE : FALSE));
      
      ++i;
    }

  /* opposite every other bit */
  i = 0;
  while (i < g_bit_vector_size(vec))
    {
      g_bit_vector_set(vec, i, (i % 2) != 0 ? TRUE : FALSE);
      
      ++i;
    }

  i = 0;
  while (i < g_bit_vector_size(vec))
    {
      g_assert(g_bit_vector_get(vec, i) == ((i % 2) != 0 ? TRUE : FALSE));
      
      ++i;
    }

  test_find(vec, size, TRUE);
  test_find(vec, size, FALSE);
}

static void
g_bit_vector_self_test(void)
{
  GBitVector* vec;
  
  vec = g_bit_vector_new();  
  g_assert(g_bit_vector_size(vec) == 0);

  /* Test lots of sizes */
  test_with_size(vec, 0);
  test_with_size(vec, 1);
  test_with_size(vec, 2);
  test_with_size(vec, 3);
  test_with_size(vec, 4);
  test_with_size(vec, 12);
  test_with_size(vec, 16);
  test_with_size(vec, 32);
  test_with_size(vec, 33);
  test_with_size(vec, 34);
  test_with_size(vec, 43);
  test_with_size(vec, 62);
  test_with_size(vec, 63);
  test_with_size(vec, 64);
  test_with_size(vec, 65);
  test_with_size(vec, 66);
  test_with_size(vec, 77);
  test_with_size(vec, 81);
  test_with_size(vec, 95);
  test_with_size(vec, 96);
  test_with_size(vec, 97);
  test_with_size(vec, 100);
  test_with_size(vec, 128);
  test_with_size(vec, 129);
  test_with_size(vec, 255);
  test_with_size(vec, 256);
  test_with_size(vec, 1000);
  test_with_size(vec, 1022);
  test_with_size(vec, 1023);
  test_with_size(vec, 1024);
  test_with_size(vec, 1025);
  test_with_size(vec, 1026);
  test_with_size(vec, 15000);

  /* Test with shrinking and duplicate sizes */
  test_with_size(vec, 15000);
  test_with_size(vec, 1026);
  test_with_size(vec, 1025);
  test_with_size(vec, 1024);
  test_with_size(vec, 1024);
  test_with_size(vec, 1023);
  test_with_size(vec, 1023);
  test_with_size(vec, 1022);
  test_with_size(vec, 1000);
  test_with_size(vec, 77);
  test_with_size(vec, 66);
  test_with_size(vec, 65);
  test_with_size(vec, 65);
  test_with_size(vec, 64);
  test_with_size(vec, 63);
  test_with_size(vec, 62);
  test_with_size(vec, 43);
  test_with_size(vec, 43);
  test_with_size(vec, 34);
  test_with_size(vec, 33);
  test_with_size(vec, 32);
  test_with_size(vec, 32);
  test_with_size(vec, 16);
  test_with_size(vec, 12);
  test_with_size(vec, 4);
  test_with_size(vec, 3);
  test_with_size(vec, 2);
  test_with_size(vec, 1);
  test_with_size(vec, 1);
  test_with_size(vec, 0);
  test_with_size(vec, 0);
}

#if 0
int
main(int argc, char** argv)
{
  g_bit_vector_self_test();

  return 0;
}
#endif
