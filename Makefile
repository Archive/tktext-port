PREFIX=/usr/local
INSTALL=/usr/bin/install -c

# If you don't want to build the canvas item (which removes the gdk-pixbuf
# dependency), comment these lines out
CANVAS_SOURCES=gtktextcanvas.c
CANVAS_OBJECTS=gtktextcanvas.o
CANVAS_TEST=testgtktextcanvas
CANVAS_CFLAGS := $(shell gnome-config --cflags gnomeui gdk_pixbuf)
CANVAS_LDFLAGS := $(shell gnome-config --libs gnomeui gdk_pixbuf)

HEADERS=gtktext.h        gtktextdisplay.h       gtktextmark.h \
gtktextbtree.h        gtktexttag.h \
gtktextbuffer.h   gtktexttagprivate.h \
gtktextchild.h   gtktextlayout.h        gtktexttypes.h gstring.h \
gbsearch.h gtktexttagtable.h gtktextiter.h gtktextiterprivate.h \
gtktextsegment.h gtktextcanvas.h gtktextmarkprivate.h

INSTALL_HEADERS=gtktext.h gtktextcanvas.h gtktextbuffer.h gtktexttagtable.h \
gtktextiter.h gtktextmark.h gtktexttag.h

LIB_SOURCES=gtktextlayout.c gtktext.c gtktextdisplay.c gtktextbtree.c gtktextbuffer.c  gtktexttag.c gtktextmark.c gtktexttypes.c gstring.c gbsearch.c \
gtktextchild.c gtktexttagtable.c gtktextiter.c gtktextsegment.c \
$(CANVAS_SOURCES)

LIB_OBJECTS=gtktextlayout.o gtktext.o gtktextdisplay.o gtktextbtree.o gtktextbuffer.o gtktexttag.o gtktextmark.o gtktexttypes.o gstring.o gbsearch.o \
gtktextchild.o gtktexttagtable.o gtktextiter.o gtktextsegment.o \
$(CANVAS_OBJECTS)

TEST_WIDGET_SOURCES=testgtktext.c $(LIB_SOURCES)

TEST_WIDGET_OBJECTS=testgtktext.o $(LIB_OBJECTS)

TEST_BUFFER_SOURCES=testgtktextbuffer.c $(LIB_SOURCES)

TEST_BUFFER_OBJECTS=testgtktextbuffer.o $(LIB_OBJECTS)

TEST_CANVAS_SOURCES=testgtktextcanvas.c $(LIB_SOURCES)

TEST_CANVAS_OBJECTS=testgtktextcanvas.o $(LIB_OBJECTS)

ALL_OBJECTS=testgtktextbuffer.o testgtktext.o testgtktextcanvas.o $(LIB_OBJECTS)

#PROFILE=-pg
PROFILE=

#EFENCE=-lefence
EFENCE=

CFLAGS=`gtk-config --cflags` `gnome-config --cflags gnomeui` $(CANVAS_CFLAGS) \
$(PROFILE)
DYN_LIBS= $(EFENCE) `gtk-config --libs` `gnome-config --libs gnomeui` -lunicode $(EFENCE) $(CANVAS_LDFLAGS)
STATIC_LIBS=-L/home/hp/local/lib -L/usr/X11R6/lib -L/home/hp/unstable/lib -rdynamic -lgmodule -ldl -lXext -lX11 -lm -lunicode ./libgtk.a ./libgdk.a ./libglib.a
MANY_STATIC_LIBS=-lunicode -L/usr/X11R6/lib -L/opt/gnome/lib -rdynamic -lgmodule -ldl -lm ./libgtk.a ./libgdk.a ./libX11.a ./libXext.a ./libXi.a ./libglib.a 

OPTIMIZE=-O2
#OPTIMIZE=

all: libgtktext.a libgtktext.so testgtktext testgtktextbuffer $(CANVAS_TEST)

testgtktext: $(TEST_WIDGET_OBJECTS)
	cc $(PROFILE) -Wall -ggdb $(OPTIMIZE) $(TEST_WIDGET_OBJECTS) $(DYN_LIBS) -o testgtktext

testgtktextbuffer: $(TEST_BUFFER_OBJECTS)
	cc $(PROFILE) -Wall -ggdb $(OPTIMIZE) $(TEST_BUFFER_OBJECTS) $(DYN_LIBS) -o testgtktextbuffer

testgtktextcanvas: $(TEST_CANVAS_OBJECTS)
	cc $(PROFILE) -Wall -ggdb $(OPTIMIZE) $(TEST_CANVAS_OBJECTS) $(DYN_LIBS) -o testgtktextcanvas

$(ALL_OBJECTS) fooedit.o: %.o: %.c $(HEADERS)
	cc -Wall -ggdb $(OPTIMIZE) -c $(CFLAGS) $< -o $@

bug: bug.c $(LIB_OBJECTS)
	cc -Wall -ggdb $(OPTIMIZE) $(CFLAGS) bug.c $(DYN_LIBS) $(LIB_OBJECTS) -o bug

# This sucks a little bit, but it won't be used for long.
libgtktext.a: $(LIB_OBJECTS)
	rm -f $@
	ar cru $@ $(LIB_OBJECTS)
	ranlib $@

libgtktext.so: $(LIB_OBJECTS)
	rm -f $@
	ld -o $@ $(LIB_OBJECTS) -shared

install: libgtktext.a libgtktext.so
	mkdir -p $(PREFIX)/include/gtktext
	@list='$(INSTALL_HEADERS)'; for p in $$list; do \
	  echo "$(INSTALL) -m 644 $$p $(PREFIX)/include/gtktext/$$p"; \
	  $(INSTALL) -m 644 $$p $(PREFIX)/include/gtktext/$$p; \
	done
	$(INSTALL) -m 644 libgtktext.a $(PREFIX)/lib/libgtktext.a
	$(INSTALL) -m 644 libgtktext.so $(PREFIX)/lib/libgtktext.so

clean: 
	/bin/rm -f *~ *.bak $(ALL_OBJECTS) libgtktext.a libgtktext.so testgtktext testgtktextbuffer $(CANVAS_TEST)

tarball:
	(cd .. && tar cvzf tktext.tar.gz tktext-port/*.[hc] tktext-port/KEYS tktext-port/README tktext-port/Makefile tktext-port/*.dia)


fooedit: fooedit.o $(LIB_OBJECTS)
	cc $(PROFILE) -Wall -ggdb $(OPTIMIZE) `gnome-config --libs gnomeui` $(LIB_OBJECTS) fooedit.o -o fooedit

DOC_MODULE=gtktext

# The top-level SGML file.
DOC_MAIN_SGML_FILE=gtktext.sgml

LDFLAGS=`gtk-config --libs` -lunicode $(LIB_OBJECTS) $(CANVAS_LDFLAGS)

CFLAGS=`gtk-config --cflags` $(CANVAS_CFLAGS)

.PHONY: scan templates sgml html

scan:
	env CFLAGS="$(CFLAGS)" LDFLAGS="$(LDFLAGS)" gtkdoc-scanobj --module=$(DOC_MODULE)
	gtkdoc-scan --module=$(DOC_MODULE) --source-dir=.

templates:
	gtkdoc-mktmpl --module=$(DOC_MODULE)

sgml:
	gtkdoc-mkdb --module=$(DOC_MODULE)

html:
	if ! test -d html ; then mkdir html ; fi
	-cd html && gtkdoc-mkhtml $(DOC_MODULE) ../$(DOC_MAIN_SGML_FILE)
