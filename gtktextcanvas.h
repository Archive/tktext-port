/* Editable gtktext item type for the GnomeCanvas
 *
 * Copyright (c) 2000 Phil Schwan
 * Copyright (c) 2000 Red Hat Software, Inc.
 *
 * Borrowing heavily from the Tk->Gtk port by Havoc Pennington <hp@redhat.com>
 * Canvas bits by Phil Schwan <pschwan@off.net>
 *
 */

#ifndef GNOME_CANVAS_RICHTEXT_H
#define GNOME_CANVAS_RICHTEXT_H

#include <libgnome/gnome-defs.h>
#include <gtk/gtkpacker.h> /* why the hell is GtkAnchorType here and not in gtkenums.h? */
#include <libgnomeui/gnome-canvas.h>
#include <libgnomeui/gnome-canvas-text.h>
#include "gtktext.h"

BEGIN_GNOME_DECLS

/* Rich text item for the canvas, derived from the canvas' text item.  All
 * relevent comments from gnome-canvas-text.h should be applied.
 *
 */

#define GNOME_TYPE_CANVAS_TKTEXT	(gnome_canvas_tktext_get_type ())
#define GNOME_CANVAS_TKTEXT(obj)	(GTK_CHECK_CAST ((obj), GNOME_TYPE_CANVAS_TKTEXT, GnomeCanvasTkText))
#define GNOME_CANVAS_TKTEXT_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GNOME_TYPE_CANVAS_TKTEXT, GnomeCanvasTkTextClass))
#define GNOME_IS_CANVAS_TKTEXT(obj)	(GTK_CHECK_TYPE ((obj), GNOME_TYPE_CANVAS_TKTEXT))
#define GNOME_IS_CANVAS_TKTEXT_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GNOME_TYPE_CANVAS_TKTEXT))

typedef struct _GnomeCanvasTkText GnomeCanvasTkText;
typedef struct _GnomeCanvasTkTextClass GnomeCanvasTkTextClass;

struct _GnomeCanvasTkText {
	GnomeCanvasItem item;

	struct _GtkTextLayout *layout;
	guint need_repaint_handler;
	GtkTextBuffer *buffer;

	gint (* selection_drag_handler) (GnomeCanvasTkText *tkxt,
					 GdkEventMotion *event);
	guint selection_drag_scan_timeout;
	gint scrolling_accel_factor;

	GtkTextMark *dnd_mark;
	guint blink_timeout;

	gdouble x, y;			/* Position at anchor */
	gdouble width;			/* Desired width (for wrapping) */
	gdouble height;			/* Desired height */
	GtkAnchorType anchor;		/* Anchor side for text */

	gdouble xofs, yofs;		/* Text offset distance from anchor */

	GdkGC *gc;                      /* GC for drawing text */

	double clip_width;		/* Width of optional clip rect */
	double clip_height;		/* Height of optional clip rect */

	int cx, cy;			/* Top-left canvas coords for text */
	int clip_cx, clip_cy;		/* Top-left canvas coords for clip */
	int clip_cwidth, clip_cheight;  /* Size of clip rectangle in pixels */

	guint clip : 1;			/* Use clip rectangle? */
	guint overwrite_mode : 1;	/* Overwrite or insert mode? */
	guint editable : 1;		/* Editable? */

	/* Should we create a new tag on next insert? */
	GdkFont * new_font;
	GdkColor * new_fg_color;
	GdkColor * new_bg_color;

	/* These are used to avoid emitting unnecessary events; the text item
	 * emits an event when the font or colour underneath the cursor
	 * changes.  These are the last values that we emitted, and are not
	 * necessarily pointers to valid structs. */
	GdkFont *last_font;
	gulong last_bg_color;
	gulong last_fg_color;

	/* FIXME: Antialias specific stuff _would_ follow */
};

struct _GnomeCanvasTkTextClass {
	GnomeCanvasItemClass parent_class;

	void (* font_changed) (GnomeCanvasTkText *tkxt, GdkFont *font);

	void (* fg_color_changed) (GnomeCanvasTkText *tkxt, guint color);

	void (* bg_color_changed) (GnomeCanvasTkText *tkxt, guint color);
};

/* Standard Gtk function */
GtkType gnome_canvas_tktext_get_type (void);

void gnome_canvas_tktext_set_buffer(GnomeCanvasTkText *tkxt,
				    GtkTextBuffer *buffer);
GSList * gnome_canvas_tktext_get_tags_at_line(GnomeCanvasTkText *tkxt,
					      gint line);
gchar * gnome_canvas_tktext_get_text_at_line(GnomeCanvasTkText *tkxt,
					     gint line);

END_GNOME_DECLS

#endif
