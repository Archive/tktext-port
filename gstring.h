
#ifndef HACKY_GSTRING_THING
#define HACKY_GSTRING_THING

#include <glib.h>

GString*
g_string_append_len(GString* string,
                    const gchar* chars,
                    guint len);

#endif
