#!/usr/bin/perl -pi.bak

s/froo_tkxt_buffer_/gtk_text_buffer_/g;
s/FrooTkxtBuffer/GtkTextBuffer/g;
s/FrooTkxtTag/GtkTextTag/g;
s/FrooTkxtStyleValues/GtkTextStyleValues/g;
s/froo_tkxt_tag_/gtk_text_tag_/g;
s/froo_tkxt_layout_/gtk_text_layout_/g;
s/froo_tkxt_btree_/gtk_text_btree_/g;
s/FrooTkxtBTree/GtkTextBTree/g;
s/FrooTkxtUniChar/GtkTextUniChar/g;
s/FrooTkxtIter/GtkTextIter/g;
s/froo_tkxt_iter_/gtk_text_iter_/g;
s/FrooTkxtChild/GtkTextChild/g;
s/FrooTkxtPixmap/GtkTextPixmap/g;
s/FrooTkxtMark/GtkTextMark/g;
s/FrooTkxtNode/GtkTextBTreeNode/g;
s/FrooTkxtLayout/GtkTextLayout/g;
s/froo_tkxt_node_/gtk_text_btree_node_/g;
s/froo_tkxt_mark_/gtk_text_mark_/g;
s/froo_tkxt_pixmap_/gtk_text_pixmap_/g;
s/froo_tkxt_line_/gtk_text_line_/g;
s/FrooTkxtLine/GtkTextLine/g;
s/FrooTkxtLineData/GtkTextLineData/g;
s/FrooTkxtTagTable/GtkTextTagTable/g;
s/froo_tkxt_tag_table_/gtk_text_tag_table_/g;
s/FrooTkxtSegment/GtkTextLineSegment/g;
s/froo_tkxt_segment_/gtk_text_line_segment_/g;
s/FrooTkxtSegType/GtkTextLineSegmentClass/g;
s/FrooTkxtToggle/GtkTextToggleBody/g;
s/FrooTkxtMarkBody/GtkTextMarkBody/g;
s/FrooTkxtTab/GtkTextTab/g;
s/FrooTkxtCounter/GtkTextCounter/g;
s/FrooTkxtTabAlign/GtkTextTabAlign/g;
s/FrooTkxtTabArray/GtkTextTabArray/g;
s/froo_tkxt_utf_/gtk_text_utf_/g;
s/froo_tkxt_latin1_/gtk_text_latin1_/g;
s/froo_tkxt_byte_/gtk_text_byte_/g;
s/froo_tkxt_unknown_/gtk_text_unknown_/g;
s/FROO_TKXT_BUFFER/GTK_TEXT_BUFFER/g;
s/FROO_TKXT_TAG/GTK_TEXT_TAG/g;
s/FROO_TKXT_TAG_TABLE/GTK_TEXT_TAG_TABLE/g;
s/FROO_TKXT_LAYOUT/GTK_TEXT_LAYOUT/g;
s/FrooTkxtEmbWindow/GtkTextEmbWindow/g;
s/FrooTkxtDisplayChunk/GtkTextDisplayChunk/g;
s/FrooTkxtIndex/GtkTextIndex/g;
s/FrooTkxtRealIter/GtkTextRealIter/g;
s/FrooTkxtDisplayLine/GtkTextDisplayLine/g;
s/FrooTkxtSegSplitFunc/GtkTextLineSegmentSplitFunc/g;
s/FrooTkxtSegLineChangeFunc/GtkTextLineSegmentLineChangeFunc/g;
s/FrooTkxtInsertDisplayFunc/GtkTextInsertDisplayFunc/g;
s/FrooTkxtWrapInfo/GtkTextDisplayLineWrapInfo/g;
s/FrooTkxtRectangle/GtkTextRectangle/g;
s/froo_tkxt_rectangle_/gtk_text_rectangle_/g;

s/froo_tkxt_/gtk_text_view_/g;
s/FrooTkxt/GtkTextView/g;
s/FROO_TYPE_TKXT/GTK_TYPE_TEXT_VIEW/g;
s/FROO_TKXT_CLASS/GTK_TEXT_VIEW_CLASS/g;
s/FROO_TKXT[ ]*\(/GTK_TEXT_VIEW\(/g;
s/FROO_IS_TKXT/GTK_IS_TEXT_VIEW/g;
s/FROO_IS_TKXT_CLASS/GTK_IS_TEXT_VIEW/g;

s/FROO_TKXT_/GTK_TEXT_/g;
s/FROO_/GTK_/g;
s/TKXT/TEXT/g;
s/_tkxt/_text/g;
s/froo_/gtk_/g;
s/Froo/Gtk/g;

s/([a-z]*)frootkxt([a-z]*)\.([hco])/\1gtktext\2.\3/g;

#  # fix * position 

#  s/([a-z])([ ]*)\*([ ]*)/\1\2\3\*/g;

# s/frootkxt\.h/gtktextview.h/g;
# s/frootxkt\.c/gtktextview.c/g;

# s/froo_tkxt_btree_get_byte_index/froo_tkxt_btree_get_line_byte_index/g;
# s/froo_tkxt_btree_get_char_index/froo_tkxt_btree_get_line_char_index/g;

#  s/TkText/FrooTkxt/g;
#  s/Tk_Seg/FrooTkxtSeg/g;
#  s/Proc/Func/g;
#  s/Tk_Chunk/FrooTkxtChunk/g;
#  s/EXTERN //g;
#  s/CONST /const /g;
#  s/tkTextDebug/froo_tkxt_enable_debug/g;
#  s/register //g;
#  s/TEXT_WRAPMODE/FROO_WRAPMODE/g;
#  s/TkBTreeCharTagged/froo_tkxt_btree_char_has_tag/g;
#  s/TkBTreeCheck/froo_tkxt_btree_check/g;
#  s/TkBTreeCharsInLine/froo_tkxt_btree_chars_in_line/g;
#  s/TkBTreeBytesInLine/froo_tkxt_btree_bytes_in_line/g;
#  s/TkBTreeCreate/froo_tkxt_btree_new/g;
#  s/TkBTreeDestroy/froo_tkxt_btree_destroy/g;
#  s/TkBTreeDeleteChars/froo_tkxt_btree_delete_chars/g;
#  s/TkBTreeFindLine/froo_tkxt_btree_find_line/g;
#  s/TkBTreeGetTags/froo_tkxt_btree_get_tags/g;
#  s/TkBTreeInsertChars/froo_tkxt_btree_insert_chars/g;
#  s/TkBTreeLineIndex/froo_tkxt_btree_line_index/g;
#  s/TkBTreeLinkSegment/froo_tkxt_btree_link_segment/g;
#  s/TkBTreeNextLine/froo_tkxt_btree_next_line/g;
#  s/TkBTreeNextTag/froo_tkxt_btree_next_tag/g;
#  s/TkBTreeNumLines/froo_tkxt_btree_num_lines/g;
#  s/TkBTreePreviousLine/froo_tkxt_btree_previous_line/g;
#  s/TkBTreePrevTag/froo_tkxt_btree_prev_tag/g;
#  s/TkBTreeStartSearch/froo_tkxt_btree_start_search/g;
#  s/TkBTreeStartSearchBack/froo_tkxt_btree_start_search_back/g;
#  s/TkBTreeTag/froo_tkxt_btree_tag/g;
#  s/TkBTreeUnlinkSegment/froo_tkxt_btree_unlink_segment/g;

#  s/ckalloc/g_malloc/g;
#  s/ckfree/g_free/g;
#  s/Tk_Offset/G_STRUCT_OFFSET/g;

#  s/FrooTkxtChanged/froo_tkxt_changed/g;
#  s/FrooTkxtCreateDInfo/froo_tkxt_create_dinfo/g;
#  s/FrooTkxtDLineInfo/froo_tkxt_dline_info/g;
#  s/FrooTkxtFreeDInfo/froo_tkxt_free_dinfo/g;
#  s/TkWrapMode/FrooWrapMode/g;
#  s/textPtr/tkxt/g;
#  s/dInfoPtr/dinfo/g;
#  s/VOID/void/g;

#  s/Tk_3DVerticalBevel/froo_tkxt_vertical_bevel/g;
#  s/Tk_3DHorizontalBevel/froo_tkxt_horizontal_bevel/g;
#  s/ClientData/gpointer/g;

#  s/TK_TAG_AFFECTS_DISPLAY/FROO_TKXT_TAG_AFFECTS_DISPLAY/g;
#  s/TK_TAG_UNDERLINE/FROO_TKXT_TAG_UNDERLINE/g;
#  s/TK_TAG_JUSTIFY/FROO_TKXT_TAG_JUSTIFY/g;
#  s/TK_TAG_OFFSET/FROO_TKXT_TAG_OFFSET/g;
#  s/tkBTreeDebug/debug_btree/g;
#  s/IncCount/inc_count/g;
#  s/Rebalance/froo_tkxt_btree_rebalance/g;
#  s/CleanupLine/cleanup_line/g;
#  s/FindTagStart/find_tag_start/g;
#  s/FrooTkxtIndexCmp/froo_tkxt_index_cmp/g;
#  s/FrooTkxtIndexToSeg/froo_tkxt_index_to_seg/g;
#  s/SplitSeg/split_segment/g;
#  s/ChangeFrooTkxtNodeToggleCount/change_node_toggle_count/g;
#  s/FindTagEnd/find_tag_end/g;
#  s/_char_tagged/_has_tag/g;
#  s/FrooTkxtIsElided/froo_tkxt_tag_is_elided/g;
#  s/CheckFrooTkxtNodeConsistency/froo_tkxt_node_check_consistency/g;
#  s/RecomputeFrooTkxtNodeCounts/recompute_node_counts/g;
#  s/CharSplitFunc/char_segment_split_func/g;
#  s/CharDeleteFunc/char_segment_delete_func/g;
#  s/CharCheckFunc/char_segment_check_func/g;
#  s/CharCleanupFunc/char_segment_cleanup_func/g;
#  s/FrooTkxtCharLayoutFunc/char_segment_layout_func/g;
#  s/CharLayoutFunc/char_segment_layout_func/g;
#  s/ToggleDeleteFunc/toggle_segment_delete_func/g;
#  s/ToggleCleanupFunc/toggle_segment_cleanup_func/g;
#  s/ToggleLineChangeFunc/toggle_segment_line_change_func/g;
#  s/ToggleCheckFunc/toggle_segment_check_func/g;
#  s/tkTextCharType/froo_tkxt_char_type/g;

#  s/tkTextRightMarkType/froo_tkxt_right_mark_type/g;
#  s/tkTextLeftMarkType/froo_tkxt_left_mark_type/g;

#  s/FrooTkxtIndexBackBytes/froo_tkxt_index_back_bytes/g;
#  s/FrooTkxtIndexBackChars/froo_tkxt_index_back_chars/g;
#  s/FrooTkxtIndexForwBytes/froo_tkxt_index_forw_bytes/g;
#  s/FrooTkxtIndexForwChars/froo_tkxt_index_forw_chars/g;

#  s/ForwBack/forward_back/g;
#  s/StartEnd/start_end/g;

#  s/FrooTkxtMakeByteIndex/froo_tkxt_btree_get_byte_index/g;
#  s/FrooTkxtMakeCharIndex/froo_tkxt_btree_get_char_index/g;

#  s/FrooTkxtSegToOffset/froo_tkxt_segment_get_offset/g;
#  s/FrooTkxtGetIndex/froo_tkxt_btree_index_from_string/g;
#  s/FrooTkxtPrintIndex/froo_tkxt_index_to_string/g;

#  s/Tcl_UniChar/FrooTkxtUniChar/g;
#  s/FrooTkxtChar/FrooTkxtUniChar/g;

#  s/Tcl_UtfPrev/froo_tkxt_utf_prev/g;
#  s/froo_txkt_utf_prev/froo_tkxt_utf_prev/g;
#  s/Tcl_UtfToUniChar/froo_tkxt_utf_to_unichar/g;
#  s/Tcl_NumUtfChars/froo_tkxt_num_utf_chars/g;

#  s/froo_tkxt_btree_line_index/froo_tkxt_btree_line_number/g;

#  s/FrooTkxtMarkSegToIndex/froo_tkxt_btree_mark_segment_to_index/g;
#  s/FrooTkxtMarkNameToIndex/froo_tkxt_btree_mark_name_to_index/g;
#  s/froo_tkxt_tag_is_elided/froo_tkxt_btree_char_is_elided/g;

#  s/MarkDeleteFunc/mark_segment_delete_func/g;
#  s/MarkCleanupFunc/mark_segment_cleanup_func/g;
#  s/MarkLayoutFunc/mark_segment_layout_func/g;
#  s/MarkCheckFunc/mark_segment_check_func/g;
#  s/MarkFindNext/mark_find_next/g;
#  s/MarkFindPrev/mark_find_prev/g;

#  s/FrooTkxtSetMark/froo_tkxt_btree_set_mark/g;
#  s/FrooTkxtDispChunk/FrooTkxtDisplayChunk/g;
#  s/froo_tkxt_changed/froo_tkxt_buffer_about_to_change/g;
#  s/froo_tkxt_buffer_changed/froo_tkxt_buffer_about_to_change/g;
#  s/ABOUT_TO_CHANGE/NEED_REDISPLAY/g;
#  s/about_to_change/need_redisplay/g;

#  s/nextPtr/next/g;
#  s/parentPtr/parent/g;
#  s/tagPtr/tag/g;
#  s/toggleCount/toggle_count/g;
#  s/linePtr/line/g;

#  s/line->segPtr/line->segments/g;

#  s/typePtr/type/g;

