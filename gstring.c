
#include "gstring.h"

#include <string.h>

/* If you look at this you'll regret it :-) */

typedef struct _GRealString      GRealString;
struct _GRealString
{
  gchar *str;
  gint   len;
  gint   alloc;
};

static inline gint
nearest_power (gint num)
{
  gint n = 1;

  while (n < num)
    n <<= 1;

  return n;
}

static void
g_string_maybe_expand (GRealString* string, gint len)
{
  if (string->len + len >= string->alloc)
    {
      string->alloc = nearest_power (string->len + len + 1);
      string->str = g_realloc (string->str, string->alloc);
    }
}


GString*
g_string_append_len(GString* fstring,
                    const gchar* val,
                    guint len)
{
  GRealString *string = (GRealString*)fstring;

  g_return_val_if_fail (string != NULL, NULL);
  g_return_val_if_fail (val != NULL, fstring);
  
  g_string_maybe_expand (string, string->len + len);

  strncpy (string->str + string->len, val, len);
  
  string->len += len;

  string->str[string->len] = '\0';
  
  return fstring;
}
