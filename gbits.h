#ifndef G_BITS_H
#define G_BITS_H

#include <glib.h>

typedef struct _GBitVector GBitVector;

GBitVector* g_bit_vector_new(void);
void        g_bit_vector_free(GBitVector* vec);

gboolean    g_bit_vector_get(GBitVector* vec, guint index);
void        g_bit_vector_set(GBitVector* vec, guint index, gboolean value);
void        g_bit_vector_set_all(GBitVector* vec, gboolean value);
void        g_bit_vector_append(GBitVector* vec, gboolean value);

void        g_bit_vector_set_size(GBitVector* vec, guint len);
guint       g_bit_vector_size(GBitVector* vec);

guint       g_bit_vector_find (GBitVector* vec, guint start, gboolean val);

#endif
