
#include "gbsearch.h"

/* In retrospect I think bsearch() is probably very portable
   and we don't need this wrapper */

gpointer
g_bsearch (gconstpointer key, gconstpointer array,
           guint n_elements, guint element_size,
           GCompareFunc compare_func)
{
#ifdef HAVE_BSEARCH
  return bsearch(key, array, n_elements, element_size, compare_func);  
#else
  /* implementation copied from GNU libc (not that it's particularly
     hard to find this implementation in any textbook :-) */
  guint l, u, idx;
  gconstpointer p;
  gint comparison;
 
  l = 0;
  u = n_elements;
  while (l < u)
    {
      idx = (l + u) / 2;
      p = (gconstpointer) (((const gchar *) array) + (idx * element_size));
      comparison = (*compare_func) (key, p);
      if (comparison < 0)
        u = idx;
      else if (comparison > 0)
        l = idx + 1;
      else
        return (gpointer) p;
    }
 
  return NULL;
#endif
}
